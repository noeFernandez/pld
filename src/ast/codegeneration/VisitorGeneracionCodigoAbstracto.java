package ast.codegeneration;

import ast.*;
import ast.expressions.*;
import ast.sentence.*;
import ast.types.*;
import ast.types.consts.ConstChar;
import ast.types.consts.FloatConst;
import ast.types.consts.IntConst;
import ast.visitor.Visitor;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public abstract class VisitorGeneracionCodigoAbstracto extends Visitor {

    public Object visit(Program node, Object param) {
        throw new NotImplementedException();
    }

    /* AbstractType Visitor methods */
    public Object visit(ArrayType node, Object param) {
        throw new NotImplementedException();

    }


    public Object visit(CharType node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(ConstChar node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(FloatConst node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(FloatType node, Object param) {
        throw new NotImplementedException();
    }

    public Object visit(IntConst node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(Incremento node, Object param) {
        throw new NotImplementedException();
    }

    public Object visit(IntegerType node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(VoidType node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(LocalVariableDeclaration node, Object param) {
        throw new NotImplementedException();
    }

    public Object visit(FieldDeclaration node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(IncrementoStatement node, Object param) {throw new NotImplementedException();}


	/* Statement Visitor methods */

    public Object visit(Assignment node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(If node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(Input node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(Print node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(While node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(ProcedureCall node, Object param) {
        throw new NotImplementedException();

    }

	/* Expression visitor methods */

    public Object visit(BinaryOperation node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(UnaryOperator node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(ArrayAccess node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(Cast node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(FieldAccess node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(FunctionCall node, Object param) {
        throw new NotImplementedException();

    }

	/*  */

    public Object visit(VariableDeclaration node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(StructType node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(Return node, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(Parameter node, Object param) {
        throw new NotImplementedException();

    }


    public Object visit(Variable variable, Object param) {
        throw new NotImplementedException();

    }

    public Object visit(FunctionDeclaration node, Object param) {
        throw new NotImplementedException();

    }

}
