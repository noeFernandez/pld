package ast.codegeneration;

import ast.*;
import ast.sentence.Sentence;
import ast.types.ArrayType;
import ast.types.StructType;
import ast.types.VoidType;
import ast.visitor.Visitor;

public class MemoryManagementVisitor extends Visitor {

    public static Integer MEMORY_OFFSET = 0;

    @Override
    public Object visit(VariableDeclaration node, Object param) {
        node.type.accept(this, null);
        node.addr = MEMORY_OFFSET;
        MEMORY_OFFSET += node.type.getSize();
        return null;

    }

    @Override
    public Object visit(LocalVariableDeclaration node, Object param){
        node.type.accept(this, null);
        return null;
    }


    @Override
    public Object visit(StructType node, Object param) {
        for (Definition field : node.fieldDeclarationList) {
            field.accept(this, null);
        }

        Integer field_offset = 0;

        for (FieldDeclaration fd : node.fieldDeclarationList) {
            fd.addr = field_offset;
            field_offset += fd.type.getSize();
        }

        return null;
    }

    @Override
    public Object visit(ArrayType node, Object param) {
       node.type.accept(this, null);
        return null;
    }

    @Override
    public Object visit(Parameter node, Object param){
        node.type.accept(this, null);
        return null;
    }

    @Override
    public Object visit(FunctionDeclaration node, Object param) {
        int LOCAL_MEMORY_OFFSET = 0;


        for (LocalVariableDeclaration vd : node.variableDeclarationList) {
            vd.accept(this, null);
            LOCAL_MEMORY_OFFSET -= vd.type.getSize();
            vd.addr = LOCAL_MEMORY_OFFSET;
        }

        for (Parameter p : node.paramList) {
            p.accept(this, null);
        }

        if (!(node.returnType instanceof VoidType)) {
            node.returnExpression.accept(this, null);
            node.returnExpression.returnExpression.accept(this, null);

        }

        Integer parameter_offset = 4;

        for (int i = node.paramList.size() - 1; i >= 0; i--) {
            Parameter p =   node.paramList.get(i);
            node.paramList.get(i).addr = parameter_offset;

            parameter_offset += node.paramList.get(i).type.getSize();
        }

        for (Sentence s : node.sentenceList) {
            s.accept(this, null);
        }

        return null;

    }

}
