package ast.codegeneration;

import ast.Definition;
import ast.LocalVariableDeclaration;
import ast.Parameter;
import ast.VariableDeclaration;
import ast.expressions.Expression;
import ast.sentence.Return;
import ast.sentence.Sentence;
import ast.types.AbstractType;
import ast.types.CharType;
import ast.types.FloatType;
import ast.types.IntegerType;

import java.util.ArrayList;
import java.util.List;

public class GeneradorDeCodigo {

    private static GeneradorDeCodigo instance;
    private List<Integer> uniqueIntegers;

    private GeneradorDeCodigo() {

        this.uniqueIntegers = new ArrayList<Integer>();

    }

    public static GeneradorDeCodigo getInstance() {
        if (instance == null)
            instance = new GeneradorDeCodigo();

        return instance;
    }

    public void call(String string) {
        System.out.println(String.format("CALL %s", string));

    }

    public void halt() {
        System.out.println("HALT");

    }

    public void store(AbstractType inferedType) {
        System.out.println(String.format("STORE%s", inferedType.suffix()));

    }

    public void in(AbstractType inferedType) {
        System.out.println(String.format("IN%s", inferedType.suffix()));

    }

    public void out(AbstractType inferedType) {
        System.out.println(String.format("OUT%s", inferedType.suffix()));

    }

    public void convertTo(AbstractType r, AbstractType l) {
        if ((r instanceof CharType) && (l instanceof IntegerType)) {
            System.out.println("B2I");
        } else if ((r instanceof CharType) && (l instanceof FloatType)) {
            System.out.println("B2I");
            System.out.println("I2F");
        } else if ((r instanceof IntegerType) && (l instanceof CharType)) {
            System.out.println("I2B");
        } else if ((r instanceof IntegerType) && (l instanceof FloatType)) {
            System.out.println("I2F");
        } else if ((r instanceof FloatType) && (l instanceof CharType)) {
            System.out.println("F2I");
            System.out.println("I2B");
        } else if ((r instanceof FloatType) && (l instanceof IntegerType)) {
            System.out.println("F2I");
        }

    }

    public void PUSH(AbstractType inferedType, String value) {
        System.out.println(String.format("PUSH%s %s", inferedType.suffix(),
                value));

    }

    public void PUSH(String value) {
        System.out.println(String.format("PUSH %s", value));

    }

    public void PUSH(AbstractType inferedType, Integer value) {
        System.out.println(String.format("PUSH%s %s", inferedType.suffix(),
                value));

    }

    public void PUSH(AbstractType inferedType, Float value) {
        System.out.println(String.format("PUSH%s %s", inferedType.suffix(),
                value));

    }

    public void load(AbstractType inferedType) {
        System.out.println(String.format("LOAD%s", inferedType.suffix()));

    }

    public void pusha(Integer addr) {
        System.out.println(String.format("PUSHA %d", addr));

    }

    public void PUSHA(String string) {
        System.out.println(String.format("PUSHA %s", string));

    }

    public void ADD(AbstractType type) {
        System.out.println(String.format("ADD%s", type.suffix()));
    }

    public void printVariableDecaration(VariableDeclaration node) {
        /**
         * SE IMRPRIME UNA DIRECTIVA TAL QUE #DATA offset /**
         * gc.declaracionVariable() que imprime el nombre y el tipo /** �int i (
         * offset: xxxx ) /** se hace para saber que pasa por aqu�
         **/
        System.out.println(String.format("'#line %d(%s %s ( offset = %d )",
                node.line, node.type.toString(), node.identifier, node.addr));

    }

    public void printVariableDecaration(LocalVariableDeclaration node) {
        /**
         * SE IMRPRIME UNA DIRECTIVA TAL QUE #DATA offset /**
         * gc.declaracionVariable() que imprime el nombre y el tipo /** �int i (
         * offset: xxxx ) /** se hace para saber que pasa por aqu�
         **/
        System.out.println(String.format("'#line %d(%s %s ( offset = %d )",
                node.line, node.type.toString(), node.identifier, node.addr));

    }

    public void operation(String operator, AbstractType type) {
        switch (operator) {
            case "+":
                ADD(type);
                break;
            case "-":
                SUB(type);
                break;
            case "%":
                MOD();
                break;
            case "/":
                DIV(type);
                break;
            case "*":
                MUL(type);
                break;
            case "&&":
                AND();
                break;
            case "||":
                OR();
                break;
            case "!":
                NOT();
                break;
            case ">":
                GT(type);
                break;
            case "<":
                LT(type);
                break;
            case ">=":
                GE(type);
                break;
            case "<=":
                LE(type);
                break;
            case "==":
                EQ(type);
                break;
            case "!=":
                NE(type);
                break;
            default:
                System.out.println(operator + "NOT IMPL");
                break;
        }

    }

    public void SUB(AbstractType type) {
        System.out.println(String.format("SUB%s", type.suffix()));

    }

    private void MOD() {
        System.out.println("MOD");

    }

    private void DIV(AbstractType type) {
        System.out.println(String.format("DIV%s", type.suffix()));

    }

    private void AND() {
        System.out.println("AND");

    }

    private void OR() {
        System.out.println("OR");

    }

    private void NOT() {
        System.out.println(String.format("NOT"));

    }

    private void GT(AbstractType type) {
        System.out.println(String.format("GT%s", type.suffix()));

    }

    private void LT(AbstractType type) {
        System.out.println(String.format("LT%s", type.suffix()));

    }

    private void GE(AbstractType type) {
        System.out.println(String.format("GE%s", type.suffix()));

    }

    private void LE(AbstractType type) {
        System.out.println(String.format("LE%s", type.suffix()));

    }

    private void NE(AbstractType type) {
        System.out.println(String.format("NE%s", type.suffix()));

    }

    private void EQ(AbstractType type) {
        System.out.println(String.format("EQ%s", type.suffix()));

    }

    public void printLable(String identifier) {
        System.out.println(String.format("%s:", identifier));

    }

    public void enter(Integer parametersSize) {
        System.out.println(String.format("ENTER %d", parametersSize));

    }

    public void ret(Integer size, int i, int j) {
        System.out.println(String.format("RET %d, %d, %d", size, i, j));

    }

    public void printFunctionParamters(List<Parameter> paramList) {
        System.out.println("' *** PARAMTERS");
        for (Parameter v : paramList) {
            printParameterDecaration(v);
        }

    }

    private void printParameterDecaration(Parameter v) {
        System.out.println(String.format("'#line %d(%s %s ( offset = %d )",
                v.line, v.type.toString(), v.identifier, v.addr));

    }

    public void printLocalVariables(
            List<LocalVariableDeclaration> variableDeclarationList) {
        System.out.println();
        System.out.println("' *** LOCAL VARIABLES");
        for (LocalVariableDeclaration v : variableDeclarationList) {
            printVariableDecaration(v);
        }

    }

    public void printVariables(List<VariableDeclaration> variableDeclarationList) {
        System.out.println();
        System.out.println("' *** LOCAL VARIABLES");
        for (VariableDeclaration v : variableDeclarationList) {
            printVariableDecaration(v);
        }

    }

    public void pop(AbstractType returnType) {
        System.out.println(String.format("POP%s", returnType.suffix()));

    }

    public void printLine(Definition node) {
        System.out.println(String.format("' #####  Line %d (Codigo: %s)",
                node.line, node.toString()));

    }

    public void printLine(Sentence node) {
        System.out.println(String.format("' #####  Line %d (Codigo: %s)",
                node.line, node.toString()));

    }

    public void printLine(Expression node) {
        System.out.println(String.format("' #####  Line %d (Codigo: %s)",
                node.line, node.toString()));

    }

    public void printLine(Return node) {
        System.out.println(String.format("' #####  Line %d (Codigo: %s)",
                node.line, node.toString()));

    }

    public void showWhatIsPrintin(Expression exp) {
        System.out
                .println(String.format("' #######  print %s ", exp.toString()));

    }

    public void MUL(AbstractType type) {
        System.out.println(String.format("MUL%s", type.suffix()));

    }

    public Integer getId() {
        if (this.uniqueIntegers.size() == 0) {
            int value = 0;
            this.uniqueIntegers.add(value);
            return value;
        } else {
            int value = this.uniqueIntegers.get(this.uniqueIntegers.size() - 1);
            value++;
            this.uniqueIntegers.add(value);
            return value;

        }

    }

    public void JMP(String string) {
        System.out.println(String.format("JMP %s", string));

    }

    public void JZ(String string) {
        System.out.println(String.format("JZ %s", string));

    }

    public void operation(String operator) {
        switch (operator) {
            case "!":
                System.out.println("NOT");
                break;
            case "-":
                System.out.println("SUB");
                break;
        }

    }

}
