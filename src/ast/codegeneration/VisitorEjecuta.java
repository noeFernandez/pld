package ast.codegeneration;

import ast.*;
import ast.expressions.Expression;
import ast.expressions.FunctionCall;
import ast.sentence.*;
import ast.types.IntegerType;
import ast.types.VoidType;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class VisitorEjecuta extends VisitorGeneracionCodigoAbstracto {

    private VisitorValor visitorValor;
    private VisitorDireccion visitorDireccion;

    public VisitorEjecuta() {
        this.visitorDireccion = new VisitorDireccion();
        this.visitorValor = new VisitorValor(visitorDireccion);
    }

    @Override
    public Object visit(Program node, Object param) {
        for (Definition definition : node.definitionList) {
            if (definition instanceof VariableDeclaration)
                definition.accept(this, null);
        }

        GeneradorDeCodigo.getInstance().call("main");
        GeneradorDeCodigo.getInstance().halt();

        for (Definition definition : node.definitionList) {
            definition.accept(this, null);
        }


        return null;

    }

    public Object visit(IncrementoStatement node, Object param) {
        /**
         * El operador ++ prefijo primero incrementa y luego evalua
         */

        /**
         * Se saca la direccion de la variable a incrementar
         */
        node.expresionAIncrementar.accept(visitorDireccion, null);

        /**
         * Se saca su valor
         */
        node.expresionAIncrementar.accept(visitorValor, null);

        /**
         * Se incrementa en una unidad
         */
        GeneradorDeCodigo.getInstance().PUSH(node.expresionAIncrementar.inferedType, 1);
        GeneradorDeCodigo.getInstance().ADD(node.expresionAIncrementar.inferedType);

        /**
         * Se guarda en la variable
         */
        GeneradorDeCodigo.getInstance().store(node.expresionAIncrementar.inferedType);

        return null;


    }


    @Override
    public Object visit(Input node, Object param) {
        for (Expression exp : node.expression) {
            exp.accept(visitorDireccion, null);
            GeneradorDeCodigo.getInstance().in(exp.inferedType);
            GeneradorDeCodigo.getInstance().store(exp.inferedType);
        }

        return null;

    }

    @Override
    public Object visit(Print node, Object param) {
        for (Expression exp : node.expression) {
            GeneradorDeCodigo.getInstance().showWhatIsPrintin(exp);
            exp.accept(visitorValor, null);
            GeneradorDeCodigo.getInstance().out(exp.inferedType);
        }

        return null;

    }

    @Override
    public Object visit(Assignment node, Object param) {

        node.left.accept(visitorDireccion, null);
        node.right.accept(visitorValor, null);
        GeneradorDeCodigo.getInstance().convertTo(node.right.inferedType,
                node.left.inferedType);
        GeneradorDeCodigo.getInstance().store(node.left.inferedType);

        return null;

    }

    @Override
    public Object visit(VariableDeclaration node, Object param) {

        GeneradorDeCodigo.getInstance().printVariableDecaration(node);
        return null;
    }

    @Override
    public Object visit(LocalVariableDeclaration node, Object param) {
        GeneradorDeCodigo.getInstance().printVariableDecaration(node);
        return null;
    }

    @Override
    public Object visit(FunctionDeclaration node, Object param) {

        /**
         * Solo lo pongo para imprimir los offsets
         */
        for (LocalVariableDeclaration v : node.variableDeclarationList) {
            v.accept(this, null);
        }

        GeneradorDeCodigo.getInstance().printLable(node.identifier);
        GeneradorDeCodigo.getInstance().enter(node.getLocalVariablesSize());

        GeneradorDeCodigo.getInstance().printFunctionParamters(node.paramList);
        GeneradorDeCodigo.getInstance().printLocalVariables(
                node.variableDeclarationList);

        for (Sentence s : node.sentenceList) {
            GeneradorDeCodigo.getInstance().printLine(s);
            s.accept(this, null);

        }

        if (node.returnType instanceof VoidType) {
            GeneradorDeCodigo.getInstance().ret(0,
                    node.getLocalVariablesSize(), node.getParametersSize());
            return null;
        }

        if (node.returnExpression.returnExpression != null) {
            GeneradorDeCodigo.getInstance().printLine(node.returnExpression);
            node.returnExpression.returnExpression.accept(visitorValor, null);
            GeneradorDeCodigo
                    .getInstance()
                    .ret(node.returnExpression.returnExpression.inferedType
                                    .getSize(),
                            node.getLocalVariablesSize(),
                            node.getParametersSize());
        } else {
            GeneradorDeCodigo.getInstance().ret(0,
                    node.getLocalVariablesSize(), node.getParametersSize());
        }

        return null;
    }


    @Override
    public Object visit(FunctionCall node, Object param) {

        node.accept(visitorValor, null);

        if (!(node.functionDefinition.returnType instanceof VoidType)) {
            GeneradorDeCodigo.getInstance().pop(
                    node.functionDefinition.returnType);
        }

        return null;

    }

    public Object visit(ProcedureCall node, Object param) {

        for (Expression exp : node.parameterList) {
            exp.accept(visitorValor, null);
        }
        GeneradorDeCodigo.getInstance().call(node.identifier);

        if (!(node.functionDefinition.returnType instanceof VoidType)) {
            GeneradorDeCodigo.getInstance().pop(
                    node.functionDefinition.returnType);
        }

        return null;

    }

    @Override
    public Object visit(While node, Object param) {

        Integer id = GeneradorDeCodigo.getInstance().getId();

        GeneradorDeCodigo.getInstance().printLable("while_" + id);
        node.condition.accept(visitorValor, null);
        GeneradorDeCodigo.getInstance().convertTo(node.condition.inferedType,
                new IntegerType());
        /**
         * Salto fuera del bucle porque no se cumple la condicion
         */
        GeneradorDeCodigo.getInstance().JZ("while_fuera_" + id);

        for (Sentence s : node.whileSentenceDeclaration) {
            GeneradorDeCodigo.getInstance().printLine(s);
            s.accept(this, null);
        }

        /**
         * Vuelta al inicio del bucle
         */
        GeneradorDeCodigo.getInstance().JMP("while_" + id);

        GeneradorDeCodigo.getInstance().printLable("while_fuera_" + id);

        return null;
    }

    @Override
    public Object visit(If node, Object param) {
        node.condition.accept(visitorValor, null);
        Integer id = GeneradorDeCodigo.getInstance().getId();
        /**
         * Si es 0 se salta al else
         */
        GeneradorDeCodigo.getInstance().JZ("else_" + id);

        for (Sentence s : node.ifSentenceDeclaration) {
            s.accept(this, null);
        }

        GeneradorDeCodigo.getInstance().JMP("fin_if_" + id);

        GeneradorDeCodigo.getInstance().printLable("else_" + id);

        for (Sentence s : node.elseSentenceDeclaration) {
            GeneradorDeCodigo.getInstance().printLine(s);
            s.accept(this, null);
        }

        GeneradorDeCodigo.getInstance().JMP("fin_if_" + id);

        GeneradorDeCodigo.getInstance().printLable("fin_if_" + id);
        return null;

    }
}
