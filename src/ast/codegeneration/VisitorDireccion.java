package ast.codegeneration;

import ast.FieldDeclaration;
import ast.expressions.ArrayAccess;
import ast.expressions.FieldAccess;
import ast.expressions.Variable;
import ast.types.IntegerType;
import ast.types.StructType;

public class VisitorDireccion extends VisitorGeneracionCodigoAbstracto {

    private VisitorValor valor = new VisitorValor(this);

    @Override
    public Object visit(Variable variable, Object param) {

        /**
         * GLOBAL
         */

        if (param == null) {
            if (variable.getAmbito() == 0) {
                GeneradorDeCodigo.getInstance().pusha(variable.definition.getOffset());
            } else {
                GeneradorDeCodigo.getInstance().PUSH("BP");
                GeneradorDeCodigo.getInstance().PUSH(new IntegerType(), variable.definition.getOffset());
                GeneradorDeCodigo.getInstance().ADD(new IntegerType());
                if (variable.isReferencedVariable()) {
                    GeneradorDeCodigo.getInstance().load(new IntegerType());
                }

            }
        } else {
            Variable parm = (Variable) param;
            variable.definition.setOffset(parm.definition.getOffset());
        }

        return null;

    }

    @Override
    public Object visit(ArrayAccess node, Object param) {
        node.fatherExpression.accept(this, null);
        node.accessorExpression.accept(valor, null);

        GeneradorDeCodigo.getInstance().convertTo(node.accessorExpression.inferedType, new IntegerType());

        GeneradorDeCodigo.getInstance().PUSH(new IntegerType(), node.inferedType.getSize());
        GeneradorDeCodigo.getInstance().MUL(new IntegerType());
        GeneradorDeCodigo.getInstance().ADD(new IntegerType());
        return null;
    }

    @Override
    public Object visit(FieldAccess node, Object param) {
        node.father.accept(this, null);

        StructType str = (StructType) node.father.inferedType;

        FieldDeclaration fieldDeclaration = str.contieneCampo(node.child);
        GeneradorDeCodigo.getInstance().PUSH(new IntegerType(), fieldDeclaration.addr);

        GeneradorDeCodigo.getInstance().ADD(new IntegerType());

        return null;
    }

}
