package ast.codegeneration;

import ast.expressions.*;
import ast.types.IntegerType;
import ast.types.consts.ConstChar;
import ast.types.consts.FloatConst;
import ast.types.consts.IntConst;

public class VisitorValor extends VisitorGeneracionCodigoAbstracto {

    private VisitorDireccion visitorDireccion;

    public VisitorValor(VisitorDireccion visitorDireccion) {
        this.visitorDireccion = visitorDireccion;
    }

    /**
     * VISITOR DE LAS constantes
     */


    @Override
    public Object visit(Incremento node, Object param) {

        /**
         * El operador ++ prefijo primero incrementa y luego evalua
         */

        /**
         * Se saca la direccion de la variable a incrementar
         */
        System.out.println(String.format("'Se saca la dirección de %s", node.expresionAIncrementar));
        node.expresionAIncrementar.accept(visitorDireccion, null);

        /**
         * Se saca su valor
         */
        System.out.println(String.format("'Se saca el valor de %s", node.expresionAIncrementar));
        node.expresionAIncrementar.accept(this, null);


        /**
         * Se incrementa en una unidad
         */
        System.out.println(String.format("'Se saca suma 1 a %s", node.expresionAIncrementar));
        GeneradorDeCodigo.getInstance().PUSH(node.inferedType, 1);
        GeneradorDeCodigo.getInstance().ADD(node.inferedType);

        /**
         * Se guarda en la variable
         */
        System.out.println(String.format("'Se guarda %s", node.expresionAIncrementar));
        GeneradorDeCodigo.getInstance().store(node.inferedType);

        /**
         * Se saca el valor aumentado
         */
        System.out.println(String.format("'Se saca el valor de %s", node.expresionAIncrementar));
        node.expresionAIncrementar.accept(this, null);


        return null;
    }

    @Override
    public Object visit(ConstChar node, Object param) {
        GeneradorDeCodigo.getInstance().PUSH(node.inferedType,
                node.getAsciiValue());
        return null;

    }

    @Override
    public Object visit(FloatConst node, Object param) {
        GeneradorDeCodigo.getInstance().PUSH(node.inferedType, node.value);
        return null;

    }

    @Override
    public Object visit(IntConst node, Object param) {
        GeneradorDeCodigo.getInstance().PUSH(node.inferedType, node.value);
        return null;

    }

    @Override
    public Object visit(Variable variable, Object param) {
        variable.accept(visitorDireccion, null);
        if (variable.definition.getType().isPrimitive())
            GeneradorDeCodigo.getInstance().load(variable.inferedType);
        return null;

    }

    @Override
    public Object visit(BinaryOperation node, Object param) {

        node.left.accept(this, null);
        GeneradorDeCodigo.getInstance().convertTo(node.left.inferedType,
                node.inferedType);
        node.right.accept(this, null);
        GeneradorDeCodigo.getInstance().convertTo(node.right.inferedType,
                node.inferedType);
        GeneradorDeCodigo.getInstance().operation(node.operator,
                node.inferedType);

        return null;

    }

    @Override
    public Object visit(UnaryOperator node, Object param) {

        if (node.operator.equals("-")) {
            GeneradorDeCodigo.getInstance().PUSH(node.getInferedType(), 0);
            node.expression.accept(this, null);
            GeneradorDeCodigo.getInstance().SUB(node.getInferedType());


        } else {
            node.expression.accept(this, null);
            GeneradorDeCodigo.getInstance().convertTo(node.expression.inferedType, new IntegerType());
            GeneradorDeCodigo.getInstance().operation(node.operator);
        }

        return null;

    }


    public Object visit(FunctionCall node, Object param) {

        for (int i = 0; i < node.parameterList.size(); i++) {
            node.parameterList.get(i).accept(this, null);
            GeneradorDeCodigo.getInstance().convertTo(node.parameterList.get(i).inferedType, node.functionDefinition.paramList.get(i).type);
        }

        GeneradorDeCodigo.getInstance().call(node.identifier);

        return null;
    }

    @Override
    public Object visit(ArrayAccess node, Object param) {
        node.accept(visitorDireccion, null);
        GeneradorDeCodigo.getInstance().load(node.fatherExpression.inferedType);
        return null;
    }

    @Override
    public Object visit(FieldAccess node, Object param) {
        node.accept(visitorDireccion, null);
        GeneradorDeCodigo.getInstance().load(node.inferedType);

        return null;
    }

    public Object visit(Cast node, Object param) {
        node.expression.accept(this, null);
        GeneradorDeCodigo.getInstance().convertTo(node.expression.inferedType,
                node.type);
        return null;

    }

}
