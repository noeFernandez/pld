package ast.sentence;

import ast.FunctionDeclaration;
import ast.expressions.Expression;
import ast.visitor.Visitor;

import java.util.List;

/**
 */
public class ProcedureCall extends Sentence {

	public String identifier;
	public List<Expression> parameterList;

	public FunctionDeclaration functionDefinition;

	@SuppressWarnings("unchecked")
	public ProcedureCall(Integer line, Object identifier, Object parameterList) {
		super(line);

		this.identifier = (String) identifier;
		this.parameterList = (List<Expression>) parameterList;
	}

	public ProcedureCall(Integer line, String identifier,
			List<Expression> parameterList) {
		super(line);

		this.identifier = identifier;
		this.parameterList = parameterList;
	}

	@Override
	public String toString() {
		String params = "";
		for (int i = 0; i < this.parameterList.size(); i++) {
			if (i == this.parameterList.size() - 1) {
				break;
			}
			params += this.parameterList.get(i).toString() + ", ";
		}
		if (this.parameterList.size() != 0)
			params += this.parameterList.get(this.parameterList.size() - 1)
					.toString();

		return String.format("%s(%s)", this.identifier, params);
	}

	@Override
	public Object accept(Visitor visitor, Object param) {
		return visitor.visit(this, null);
	}

}
