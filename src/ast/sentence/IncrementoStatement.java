package ast.sentence;

import ast.expressions.Expression;
import ast.visitor.Visitor;

import java.beans.Statement;

/**
 * Created by noe Fern�ndez �lvarez on 03/07/2015.
 * noe.fernandez@treelogic.com
 */
public class IncrementoStatement extends Sentence {

    public Expression expresionAIncrementar;


    public IncrementoStatement(Integer line, Expression expression) {
        super(line);
        this.expresionAIncrementar = expression;
    }

    public IncrementoStatement(Integer line, Object expression) {
        super(line);
        this.expresionAIncrementar = (Expression) expression;
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, param);
    }


    @Override
    public String toString() {
        return "++" + expresionAIncrementar;
    }
}
