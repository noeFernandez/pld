package ast.sentence;

import ast.expressions.Expression;
import ast.visitor.Visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Noé on 3/17/2015.
 */
public class If extends Sentence {

    public Expression condition;
    public List<Sentence> ifSentenceDeclaration;
    public List<Sentence> elseSentenceDeclaration;


	@SuppressWarnings("unchecked")
 public If(Integer line,Object condition, Object ifSentenceDeclaration, Object elseSentenceDeclaration){
    	super(line);
        this.condition = (Expression) condition;

        this.ifSentenceDeclaration = new ArrayList<Sentence>();
        this.elseSentenceDeclaration = new ArrayList<Sentence>();

        if(ifSentenceDeclaration != null && ifSentenceDeclaration instanceof Sentence){
                this.ifSentenceDeclaration.add((Sentence) ifSentenceDeclaration);

        }

        if(elseSentenceDeclaration != null && elseSentenceDeclaration instanceof Sentence){
                this.elseSentenceDeclaration.add((Sentence) elseSentenceDeclaration);

        }

        if(ifSentenceDeclaration != null && ifSentenceDeclaration instanceof ArrayList){
            this.ifSentenceDeclaration = (List<Sentence>) ifSentenceDeclaration;
        }

        if(elseSentenceDeclaration != null && elseSentenceDeclaration instanceof ArrayList){
            this.elseSentenceDeclaration = (List<Sentence>) elseSentenceDeclaration;
        }



}

    public If(Integer line,Expression condition,  List<Sentence> ifSentenceDeclaration, List<Sentence> elseSentenceDeclaration){
    	super(line);
        this.condition = condition;
        this.ifSentenceDeclaration = ifSentenceDeclaration;
        this.elseSentenceDeclaration = elseSentenceDeclaration;
    }


    public If(Integer line,Expression condition , Sentence ifSentenceDeclaration, Sentence elseSentenceDeclaration){
    	super(line);
        this.ifSentenceDeclaration = new ArrayList<Sentence>();
        this.elseSentenceDeclaration = new ArrayList<Sentence>();


        this.ifSentenceDeclaration.add(ifSentenceDeclaration);
        this.elseSentenceDeclaration.add(elseSentenceDeclaration);
        this.condition = condition;
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

	@Override
	public String toString() {
        return this.line + "";
    }
}
