package ast.sentence;

import ast.expressions.Expression;
import ast.visitor.Visitor;

/**
 * Created by Noé on 3/17/2015.
 */
public class Return extends Sentence {

    public Integer line;
    public Expression returnExpression;

    public Return(Integer line, Expression expression) {
        super(line);
        this.returnExpression = expression;
    }

    @Override
    public String toString() {
        return "return " + returnExpression;
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }
}
