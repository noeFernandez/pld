package ast.sentence;

import ast.AST;

/**
 * Created by Noé on 3/17/2015.
 */
public abstract class Sentence extends AST {

	public Integer line;

	public Sentence(Integer line) {
		super(line);
	}

}
