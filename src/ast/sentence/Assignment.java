package ast.sentence;

import ast.expressions.Expression;
import ast.visitor.Visitor;

/**
 * Created by Noé on 3/24/2015.
 */
public class Assignment extends Sentence {

	public Expression right;
	public Expression left;

	public Assignment(Integer line, Object left, Object right) {
		super(line);
		this.right = (Expression) right;
		this.left = (Expression) left;
	}

	public Assignment(Integer line, Expression left, Expression right) {
		super(line);
		this.right = right;
		this.left = left;
	}

	@Override
	public String toString() {
		return String.format("%s = %s", left.toString(), right.toString());
	}

	@Override
	public Object accept(Visitor visitor, Object param) {
		return visitor.visit(this, null);
	}
}
