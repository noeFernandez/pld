package ast.sentence;

import ast.expressions.Expression;
import ast.visitor.Visitor;

import java.util.List;

/**
 * Created by Noé on 3/17/2015.
 */
public class Print extends Sentence {

	public List<Expression> expression;

	@SuppressWarnings("unchecked")
	public Print(Integer line, Object expression) {
		super(line);

		this.expression = (List<Expression>) expression;
	}

	public Print(Integer line, List<Expression> expression) {
		super(line);

		this.expression = expression;
	}

	@Override
	public String toString() {
		String res = "print ";
		for (Expression s : expression) {
			res += s.toString() + ", ";
		}
		return res += ";";
	}

	@Override
	public Object accept(Visitor visitor, Object param) {
		return visitor.visit(this, null);
	}

}
