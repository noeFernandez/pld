package ast.sentence;

import ast.expressions.Expression;
import ast.visitor.Visitor;

import java.util.List;

/**
 * Created by Noé on 3/17/2015.
 */
public class Input extends Sentence {

    public List<Expression> expression;

    @SuppressWarnings("unchecked")
	public Input(Integer line,Object expression){
    	super(line);
        this.expression = (List<Expression>) expression;
    }

    public Input(Integer line,List<Expression> expression){
    	super(line);
        this.expression =expression;
    }

    @Override
	public String toString() {
    	String res = "raw_input ";
    	for(Expression s : expression){
    		res += s.toString() + ", ";
    	}
		return res += ";";
	}

	@Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

}
