package ast.sentence;

import ast.VariableDeclaration;
import ast.expressions.Expression;
import ast.visitor.Visitor;

import java.util.List;

/**
 * Created by Noé on 3/17/2015.
 */
public class While extends Sentence {

	public Expression condition;
	public List<VariableDeclaration> whileVariableDeclaration;
	public List<Sentence> whileSentenceDeclaration;

	@SuppressWarnings("unchecked")
	public While(Integer line, Object condition,
			Object whileVariableDeclaration, Object whileSentenceDeclaration) {
		super(line);

		this.condition = (Expression) condition;
		this.whileVariableDeclaration = (List<VariableDeclaration>) whileVariableDeclaration;
		this.whileSentenceDeclaration = (List<Sentence>) whileSentenceDeclaration;
	}

	public While(Integer line, Expression condition,
			List<VariableDeclaration> whileVariableDeclaration,
			List<Sentence> whileSentenceDeclaration) {
		super(line);

		this.condition = condition;
		this.whileVariableDeclaration = whileVariableDeclaration;
		this.whileSentenceDeclaration = whileSentenceDeclaration;
	}

	@Override
	public String toString() {
		return "" + this.line;
	}

	@Override
	public Object accept(Visitor visitor, Object param) {
		return visitor.visit(this, null);
	}

}
