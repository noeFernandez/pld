package ast.types;

import ast.Definition;
import ast.FieldDeclaration;
import ast.visitor.Visitor;
import errors.TipoError;

import java.util.List;

/**
 * Created by Noé on 3/12/2015.
 */
public class StructType extends AbstractType {

    public List<FieldDeclaration> fieldDeclarationList;

    @SuppressWarnings("unchecked")
    public StructType(Object variableDeclarations) {
        this.fieldDeclarationList = (List<FieldDeclaration>) variableDeclarations;
    }

    public StructType(List<FieldDeclaration> variableDeclarationList) {
        this.fieldDeclarationList = variableDeclarationList;
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

    public FieldDeclaration contieneCampo(String id) {
        for (Definition d : fieldDeclarationList) {
            if (d.identifier.equals(id)) {
                return (FieldDeclaration) d;
            }
        }

        return null;
    }

    @Override
    public boolean isPrimitive() {

        return false;
    }


    @Override
    public String suffix() {
        throw new IllegalAccessError("Las estructuras no son tios simples");
    }

    @Override
    public AbstractType promociona(AbstractType other) {
        return new TipoError("Una estructura nunca promociona a otro tipo", this);
    }

    @Override
    public AbstractType cast(AbstractType type) {
        return new TipoError("Solo se permite castear a tipos primitivos", this);
    }

    @Override
    public AbstractType unaryExpression(String operator) {
        return new TipoError("No se puede aplicar una estructura a una expresion unaria", this);
    }

    @Override
    public AbstractType binaryExpression(String operator, AbstractType param2) {
        return new TipoError("No se puede aplicar una estructura a una expresion biaria", this);

    }

    @Override
    public Integer getSize() {
        int sum = 0;
        for (FieldDeclaration vd : this.fieldDeclarationList) {
            sum += vd.type.getSize();
        }

        return sum;
    }

    public FieldDeclaration getField(String identifier) {
        for (FieldDeclaration fieldDeclaration : this.fieldDeclarationList) {
            if (fieldDeclaration.identifier.equals(identifier)) {
                return fieldDeclaration;
            }
        }
        return null;
    }


    @Override
    public String toString() {
        return "StructType{" +
                "fieldDeclarationList=" + fieldDeclarationList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        StructType s = (StructType) o;

        if (this.fieldDeclarationList.size() != s.fieldDeclarationList.size()) {
            return false;
        }

        for (FieldDeclaration fd : this.fieldDeclarationList) {

            FieldDeclaration fdT = s.getField(fd.identifier);
            if (fdT != null) {
                System.out.println(fd.type.equals(fdT.type));
                System.out.println(fd.type);
                System.out.println(fdT.type);
                return fd.type.equals(fdT.type);
            } else {
                return false;
            }
        }

        return false;
    }
}
