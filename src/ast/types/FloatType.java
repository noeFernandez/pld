package ast.types;

import ast.visitor.Visitor;
import errors.TipoError;

/**
 * Created by Noé on 3/18/2015.
 */
public class FloatType extends AbstractType {

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

    @Override
    public boolean isPrimitive() {
        return true;
    }

    @Override
    public AbstractType promociona(AbstractType other) {
        if (other instanceof FloatType) {
            return new FloatType();
        } else {
            return new TipoError("Float no promociona a " + other, this);
        }

    }

    @Override
    public AbstractType cast(AbstractType type) {
        if (type.isPrimitive()) {
            return type;
        }

        return new TipoError("Solo se permiten casteos a tipos primitivos",
                this);
    }

    @Override
    public AbstractType unaryExpression(String operator) {
        if (isAritmetic(operator)) {
            return this;
        } else {
            return new TipoError(
                    "Tipo no valido", this);
        }
    }

    @Override
    public AbstractType binaryExpression(String operator, AbstractType param2) {
        if (isAritmetic(operator)) {
            /**
             * Hay que comprobar el tipo del operador de resto porque solo
             * permite numeros enteros int % int
             */

            if (operator.equals("%")) {
                return new TipoError(
                        "No se permite aplica un tipo real al operador %", this);
            }

            // el tipo de retrno de la expresion binaria ser� el mayor de los
            // dos
            return getUpperType(param2);

        } else if (isLogical(operator)) {

            return new TipoError(
                    "No se puede aplicar un tipo real a una expresion logica",
                    this);

        } else if (isRelational(operator)) {
            if (!(getUpperType(param2) instanceof TipoError)) {
                return new IntegerType();
            }
            return new TipoError(
                    "Tipos no compatibles para una expresion relacional", this);
        } else {
            return new TipoError(
                    "Tipos no compatibles para una expresion binaria", this);
        }
    }

    public String suffix() {
        return "F";
    }

    @Override
    public Integer getSize() {
        return 4;
    }

    @Override
    public String toString() {
        return "float";

    }


    @Override
    public AbstractType castImplicito(AbstractType otro) {

        /**
         * Todos los tipos primitivos promocionan a float
         */
        if (!(otro instanceof FloatType) && !(otro instanceof IntegerType)
                && !(otro instanceof CharType))
            return null;
        return this;
    }

}
