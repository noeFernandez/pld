package ast.types;

import ast.AST;
import errors.TipoError;

/**
 * Created by Noé on 3/18/2015.
 */
public abstract class AbstractType extends AST {

    public AbstractType() {
        super(0);
    }

    public AbstractType corchetes(AbstractType indice) {
        return new TipoError(String.format("%s no es un array o su indice no es entero", indice), this);
    }

    public abstract Integer getSize();

    public abstract boolean isPrimitive();

    public abstract AbstractType promociona(AbstractType other);

    public abstract AbstractType cast(AbstractType type);

    public abstract AbstractType unaryExpression(String operator);

    public abstract AbstractType binaryExpression(String operator, AbstractType param2);

    public String suffix() {
        return "ERROR";
    }

    public boolean isLogical(String operator) {

        switch (operator) {
            case "&&":
                return true;
            case "||":
                return true;
            case "!":
                return true;
            default:
                return false;
        }

    }

    public boolean isAritmetic(String operator) {
        switch (operator) {
            case "+":
                return true;
            case "-":
                return true;
            case "/":
                return true;
            case "*":
                return true;
            case "%":
                return true;
            default:
                return false;
        }

    }

    public boolean isRelational(String operator) {
        switch (operator) {
            case ">":
                return true;
            case "<":
                return true;
            case "==":
                return true;
            case ">=":
                return true;
            case "<=":
                return true;
            case "!=":
                return true;
            default:
                return false;
        }
    }

    public AbstractType getUpperType(AbstractType otherType) {

        // Si el otro promociona a mi, yo soy mayor o igual que el.
        if (this.castImplicito(otherType) != null) {
            return this;
            // Si yo promociono al otro, el es mayor o igual que yo.
        } else if (otherType.castImplicito(this) != null) {
            return otherType.promociona(this);
        }
        return new TipoError(otherType + " no es compatible con " + this, this);

    }


    public AbstractType castImplicito(AbstractType otro) {
        return null;
    }

}
