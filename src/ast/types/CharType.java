package ast.types;

import ast.visitor.Visitor;
import errors.TipoError;

/**
 * Created by Noé on 3/18/2015.
 */
public class CharType extends AbstractType {

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

    @Override
    public boolean isPrimitive() {
        return true;
    }

    @Override
    public AbstractType promociona(AbstractType other) {
        if (other instanceof IntegerType) {
            return new IntegerType();
        } else if (other instanceof FloatType) {
            return new FloatType();
        } else if (other instanceof CharType) {
            return new CharType();
        } else {
            return new TipoError("Char no promociona a " + other, this);
        }
    }

    @Override
    public AbstractType cast(AbstractType type) {
        if (type.isPrimitive()) {
            return type;
        }

        return new TipoError("Solo se permiten casteos a tipos primitivos",
                this);
    }

    @Override
    public AbstractType unaryExpression(String operator) {
       return this;
    }

    @Override
    public AbstractType binaryExpression(String operator, AbstractType param2) {
        if (isAritmetic(operator)) {
            /**
             * Hay que comprobar el tipo del operador de resto porque solo
             * permite numeros enteros int % int
             */

            if (operator.equals("%")) {
                return new TipoError("Tipo no permitido con el operador %", this);
            }

            return getUpperType(param2);


        } else if (isRelational(operator)) {
            if (!(getUpperType(param2) instanceof TipoError)) {
                return new IntegerType();
            }

            return new TipoError(
                    "Tipos no compatibles para una expresion relacional", this);
        }


        return new TipoError(
                "Tipos no compatibles para una expresion binaria", this);

    }

    public String suffix() {
        return "B";
    }


    @Override
    public Integer getSize() {

        return 1;
    }

    @Override
    public String toString() {
        return "char";

    }

    @Override
    public AbstractType castImplicito(AbstractType otro) {
        if (!(otro instanceof CharType))
            return null;
        return this;
    }

}
