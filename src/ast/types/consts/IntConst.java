package ast.types.consts;

import ast.expressions.Expression;
import ast.visitor.Visitor;

/**
 * Created by Noé on 3/17/2015.
 */
public class IntConst extends Expression {

    public Integer value;

    public IntConst(Integer line, Object value){
    	super(line);
        this.value =  Integer.valueOf((String) value);
    }


    public IntConst(Integer line,Integer value){
    	super(line);
        this.value = value;
    }

    @Override
	public String toString() {
		return value.toString();
	}


	@Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }


}
