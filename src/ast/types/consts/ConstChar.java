package ast.types.consts;

import ast.expressions.Expression;
import ast.visitor.Visitor;
import errors.ME;
import errors.TipoError;

/**
 * Created by Noé on 3/18/2015.
 */
public class ConstChar extends Expression {

	public String value;
	private int asciiValue;

	public ConstChar(Integer line, Object value) {
		super(line);
		this.value = ((String) value);
		String sinComillas = ((String) value).substring(1,
				((String) value).length() - 1);
		char[] cs = (sinComillas).toCharArray();

		if (cs.length > 1) {
		
			switch (sinComillas) {
			case "\\n":
				asciiValue = 10;
				break;
			default:
				ME.getME()
						.addError(
								new TipoError(value
										+ " no es un caracter valido", this));
				break;

			}

		} else {
			asciiValue = cs[0];
		}

	}

	public ConstChar(Integer line, String value) {
		super(line);

		this.value = value;
		String sinComillas = ((String) value).substring(1,
				((String) value).length() - 1);
		char[] cs = (sinComillas).toCharArray();

		if (cs.length > 1) {
			switch (sinComillas) {
			case "\\n":
				asciiValue = 10;
				break;
			default:
				ME.getME()
						.addError(
								new TipoError(value
										+ " no es un caracter valido", this));
				break;

			}
		} else {
			asciiValue = cs[0];
		}

	}

	@Override
	public String toString() {
		return value.toString();
	}

	@Override
	public Object accept(Visitor visitor, Object param) {
		return visitor.visit(this, null);
	}

	public int getAsciiValue() {
		return asciiValue;
	}

}
