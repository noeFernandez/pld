package ast.types.consts;

import ast.expressions.Expression;
import ast.visitor.Visitor;

/**
 * Created by Noé on 3/17/2015.
 */
public class FloatConst extends Expression {

    public Float value;

    public FloatConst(Integer line,Object value){
    	super(line);
        this.value = Float.valueOf((String) value);
    }


    public FloatConst(Integer line,Float value){
    	super(line);
        this.value = value;
    }
    
    @Override
   	public String toString() {
   		return value.toString();
   	}

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }
    

}
