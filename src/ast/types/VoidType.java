package ast.types;

import ast.visitor.Visitor;
import errors.TipoError;

/**
 * Created by Noé on 3/18/2015.
 */
public class VoidType extends AbstractType {

	@Override
	public Object accept(Visitor visitor, Object param) {
		return visitor.visit(this, null);
	}

	@Override
	public boolean isPrimitive() {
		return true;
	}

	@Override
	public AbstractType promociona(AbstractType other) {

		return new TipoError("void no promociona a " + other, this);

	}

	@Override
	public AbstractType cast(AbstractType type) {

		return new TipoError("El tipo void no se puede castar", this);
	}

	@Override
	public AbstractType unaryExpression(String operator) {
		return new TipoError(
				"No se puede aplicar un oeprador a un elemento tipo void", this);
	}

	@Override
	public AbstractType binaryExpression(String operator, AbstractType param2) {
		return new TipoError(
				"No se puede aplicar un oeprador a un elemento tipo void", this);
	}

	@Override
	public Integer getSize() {
		return 0;
	}
}
