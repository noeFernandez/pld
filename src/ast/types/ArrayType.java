package ast.types;

import ast.visitor.Visitor;
import errors.TipoError;

/**
 * Created by Noé on 3/23/2015.
 */
public class ArrayType extends AbstractType {

    public AbstractType type;
    public Integer length;

    public ArrayType(Object type, Object length) {
        this.type = (AbstractType) type;
        this.length = (Integer.valueOf((String) length));
    }

    public ArrayType(AbstractType type, Integer length) {
        this.type = type;
        this.length = length;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        ArrayType other = (ArrayType) obj;
        return this.toString().equals(other.toString());
    }

    @Override
    public String toString() {
        return "ArrayType{" +
                "type=" + type +
                ", length=" + length +
                '}';
    }

    public String suffix() {
        return type.suffix();
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

    @Override
    public AbstractType corchetes(AbstractType t) {
        if (!((t).promociona(new IntegerType()) instanceof IntegerType))
            return new TipoError("Indice de acceso a array te tipo no valido",
                    this);
        return type;
    }

    @Override
    public boolean isPrimitive() {
        return false;
    }

    @Override
    public AbstractType promociona(AbstractType other) {
        return new TipoError("Un array no puede promocionar a otro tipo", this);
    }

    @Override
    public AbstractType cast(AbstractType type) {
        return new TipoError("Los arrays no pueden ser castados", this);
    }

    @Override
    public AbstractType unaryExpression(String operator) {
        return new TipoError(
                "No se puede aplicar un array a una expresion unaria", this);

    }

    @Override
    public AbstractType binaryExpression(String operator, AbstractType param2) {
        return new TipoError(
                "No se puede aplicar un array a una expresion binaria", this);

    }

    @Override
    public Integer getSize() {

        return type.getSize() * length;
    }


}
