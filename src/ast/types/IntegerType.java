package ast.types;

import ast.visitor.Visitor;
import errors.TipoError;

/**
 * Created by Noé on 3/18/2015.
 */
public class IntegerType extends AbstractType {

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

    @Override
    public boolean isPrimitive() {
        return true;
    }

    @Override
    public AbstractType promociona(AbstractType other) {
        if (other instanceof CharType) {
            return new CharType();
        } else if (other instanceof FloatType) {
            return new FloatType();
        } else if (other instanceof IntegerType) {
            return new IntegerType();
        } else {
            return new TipoError("Int no promociona a " + other, this);
        }
    }

    @Override
    public AbstractType cast(AbstractType type) {
        if (type.isPrimitive()) {
            return type;
        }

        return new TipoError("Solo se permiten casteos a tipos primitivos", this);
    }

    @Override
    public AbstractType unaryExpression(String operator) {
       return this;
    }

    @Override
    public AbstractType binaryExpression(String operator, AbstractType param2) {

        if (isAritmetic(operator)) {
            /**
             * Hay que comprobar el tipo del operador de resto porque solo
             * permite numeros enteros int % int
             */

            if (operator.equals("%")) {
                if (!((param2.promociona(new IntegerType())) instanceof TipoError)) {
                    return this;
                } else {
                    return new TipoError("El operador de modulo solo permite numeros enteros", this);
                }
            }
            // el tipo de retrno de la expresion binaria ser� el mayor de los dos
            return getUpperType(param2);

        } else if (isLogical(operator)) {
            return getUpperType(param2);
        } else if (isRelational(operator)) {
            return getUpperType(param2);
        } else {
            return new TipoError("Tipos no compatibles para una expresion binaria", this);
        }
    }

    @Override
    public AbstractType corchetes(AbstractType indice) {
        return indice;
    }

    public String suffix() {
        return "I";
    }

    @Override
    public Integer getSize() {
        return 2;
    }

    @Override
    public String toString() {
        return "int";
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof IntegerType;
    }

    @Override
    public AbstractType castImplicito(AbstractType otro) {
        // El unico que no promociona a int es float.
        if (!(otro instanceof IntegerType) && !(otro instanceof CharType))
            return null;
        return this;
    }
}
