package ast.expressions;

import ast.visitor.Visitor;

/**
 * Created by Noé on 3/17/2015.
 */
public class BinaryOperation extends Expression {

	public Expression left;
	public String operator;
	public Expression right;

	public BinaryOperation(Integer line,Object left, Object operator, Object right) {
		super(line);
		this.left = (Expression) left;
		this.operator = (String) operator;
		this.right = (Expression) right;
	}

	public BinaryOperation(Integer line,Expression left, String operator, Expression right) {
		super(line);
		this.left = left;
		this.operator = operator;
		this.right = right;
	}

	@Override
	public Object accept(Visitor visitor, Object param) {
		return visitor.visit(this, null);
	}

	public boolean isAritmetica() {
		switch (operator) {
		case "+":
			return true;
		case "-":
			return true;
		case "%":
			return true;
		case "/":
			return true;
		case "*":
			return true;
		default:
			return false;
		}

	}
	
	public boolean isLogica() {
		switch (operator) {
		case "&&":
			return true;
		case "||":
			return true;
		case "!":
			return true;
		default:
			return false;
		}

	}
	
	public boolean isComparacion() {
		switch (operator) {
		case ">":
			return true;
		case "<":
			return true;
		case ">=":
			return true;
		case "<=":
			return true;
		case "==":
			return true;
		default:
			return false;
		}

	}

	@Override
	public String toString() {
		return String.format("%s %s %s", left, operator, right);
	}
}
