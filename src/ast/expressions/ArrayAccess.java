package ast.expressions;

import ast.visitor.Visitor;

/**
 * Created by Noé on 3/17/2015.
 */
public class ArrayAccess extends Expression {

    public Expression fatherExpression;
    public Expression accessorExpression;

    public ArrayAccess(Integer line,Object fatherExpression, Object accessorExpression){
    	super(line);
        this.fatherExpression = (Expression) fatherExpression;
        this.accessorExpression = (Expression) accessorExpression;
    }

	public ArrayAccess(Integer line,Expression fatherExpression, Expression accessorExpression){
    	super(line);
        this.fatherExpression = fatherExpression;
        this.accessorExpression = accessorExpression;
    }

    @Override
    public String toString() {
        return "ArrayAccess [fatherExpression=" + fatherExpression
                + ", accessorExpression=" + accessorExpression + "]";
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

}
