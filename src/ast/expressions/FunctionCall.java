package ast.expressions;

import ast.FunctionDeclaration;
import ast.visitor.Visitor;

import java.util.List;

/**
 * Created by Noé on 3/17/2015.
 */
public class FunctionCall extends Expression {

	public String identifier;
	public List<Expression> parameterList;

	public FunctionDeclaration functionDefinition;

	@SuppressWarnings("unchecked")
	public FunctionCall(Integer line, Object identifier, Object parameterList) {
		super(line);
		this.identifier = (String) identifier;
		this.parameterList = (List<Expression>) parameterList;
	}

	public FunctionCall(Integer line, String identifier,
			List<Expression> parameterList) {
		super(line);
		this.identifier = identifier;
		this.parameterList = parameterList;
	}

	@Override
	public String toString() {
		return "FunctionCall [identifier=" + identifier + ", parameterList="
				+ parameterList + ", functionDefinition=" + functionDefinition
				+ "]";
	}

	@Override
	public Object accept(Visitor visitor, Object param) {
		return visitor.visit(this, null);
	}

}
