package ast.expressions;

import ast.types.AbstractType;
import ast.visitor.Visitor;

/**
 * Created by Noé on 3/24/2015.
 */
public class Cast extends Expression {

    public Expression expression;
    public AbstractType type;


    public Cast(Integer line,Object expression, Object type){
    	super(line);
        this.expression = (Expression) expression;
        this.type = (AbstractType) type;
    }

    public Cast(Integer line, Expression expression, AbstractType type) {
        super(line);
        this.expression = expression;
        this.type = type;
    }


    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

	@Override
	public String toString() {
		return "Cast [expression=" + expression + ", type=" + type + "]";
	}
}
