package ast.expressions;

import ast.visitor.Visitor;

/**
 * Created by Noé on 3/17/2015.
 */
public class UnaryOperator extends Expression {

	public String operator;
	public Expression expression;

	public UnaryOperator(Integer line, Object operator, Object expression) {
		super(line);
		this.operator = (String) operator;
		this.expression = (Expression) expression;
	}

	public UnaryOperator(Integer line, String operator, Expression expression) {
		super(line);
		this.operator = operator;
		this.expression = expression;
	}

	@Override
	public String toString() {
		return "UnaryOperator [operator=" + operator + ", expression="
				+ expression + "]";
	}

	@Override
	public Object accept(Visitor visitor, Object param) {
		return visitor.visit(this, null);
	}

}
