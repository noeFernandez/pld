package ast.expressions;

import ast.Definition;
import ast.visitor.Visitor;

public class Variable extends Expression {

    public String identifier;
    public Definition definition;

    public Variable(Integer line, String ident) {
        super(line);
        this.identifier = ident;

    }

    public Variable(Integer line, Object ident) {
        super(line);
        this.identifier = (String) ident;
    }

    @Override
    public String toString() {
        return this.identifier;
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, param);
    }

    public Integer getOffset() {

        return definition.isReference ? 2 : definition.getOffset();
    }

    public Integer getAmbito() {
        return definition.ambito;
    }

    public boolean isReferencedVariable() {
        return definition.isReference;
    }

}
