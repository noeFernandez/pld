package ast.expressions;

import ast.visitor.Visitor;

/**
 * Created by Noé on 3/17/2015.
 */
public class FieldAccess extends Expression {

    public Expression father;
    public String child;

    public FieldAccess(Integer line,Object father, Object child){
    	super(line);
        this.father = (Expression) father;
        this.child = (String) child;
    }

    public FieldAccess(Integer line,Expression father, String child){
    	super(line);
        this.father = father;
        this.child = child;
    }

    @Override
	public String toString() {
		return String.format("%s.%s",father,child);
	}

	@Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

}
