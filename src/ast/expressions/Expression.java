package ast.expressions;

import ast.AST;
import ast.types.AbstractType;

/**
 * Created by Noé on 3/17/2015.
 */
public abstract class Expression extends AST {

	public Integer line;
	public Boolean lvalue;
	public AbstractType inferedType;

	public Expression(Integer line) {
		super(line);
		this.lvalue = true;
		// TODO Auto-generated constructor stub
	}

	public AbstractType getInferedType() {
		return inferedType;
	}

	public void setInferedType(AbstractType inferedType) {
		this.inferedType = inferedType;
	}

	public Boolean getLvalue() {
        return lvalue;
    }

    public void setLvalue(Boolean lvalue) {
        this.lvalue = lvalue;
    }
    
    
}
