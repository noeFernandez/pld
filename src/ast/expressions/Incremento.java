package ast.expressions;

import ast.visitor.Visitor;

/**
 * Created by noe Fern�ndez �lvarez on 03/07/2015.
 * noe.fernandez@treelogic.com
 */
public class Incremento extends Expression {

    public Expression expresionAIncrementar;


    public Incremento(Integer line, Expression expression) {
        super(line);
        this.expresionAIncrementar = expression;
    }

    public Incremento(Integer line, Object expression) {
        super(line);
        this.expresionAIncrementar = (Expression) expression;
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, param);
    }


    @Override
    public String toString(){
        return "++"+expresionAIncrementar;
    }
}
