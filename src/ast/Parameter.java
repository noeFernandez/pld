package ast;


import ast.types.AbstractType;
import ast.visitor.Visitor;

/**
 * Created by Noé on 3/12/2015.
 */
public class Parameter extends Definition {

    public AbstractType type;
    public Integer addr;
    public Integer ambito;

    public Parameter(Integer line, Object type, Object identifier) {
        super(line);
        this.type = (AbstractType) type;
        this.identifier = (String) identifier;

    }

    public Parameter(Integer line, AbstractType type, String identifier) {
        super(line);

        this.type = type;
        this.identifier = identifier;
    }


    public String toString() {
        return String.format("%s %s", type, identifier);
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

    @Override
    public AbstractType getType() {
        return type;
    }


    @Override
    public Integer getOffset() {
        return addr;
    }

    @Override
    public void setOffset(Integer offset) {
        this.addr = offset;

    }

    public Integer getSize() {
        return isReference ? 2 : type.getSize();
    }

}
