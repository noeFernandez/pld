package ast;


import ast.types.AbstractType;
import ast.visitor.Visitor;

/**
 * Created by Noé on 3/12/2015.
 */
public class FieldDeclaration extends Definition {

    public AbstractType type;
    public Integer addr;

    public FieldDeclaration(Integer line,Object type, Object identifier){
    	super(line);
        this.type = (AbstractType) type;
        this.identifier = (String) identifier;
    }

    public FieldDeclaration(Integer line, AbstractType type, String identifier) {
        super(line);
        this.type =  type;
        this.identifier =  identifier;
    }


    public String toString(){
        return String.format("%s (+%d) %s", type,addr,identifier);
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

	@Override
    public AbstractType getType() {
        return this.type;
	}

	@Override
	public Integer getOffset() {
		return this.addr;
	}

	@Override
	public void setOffset(Integer offset) {
		this.addr = offset;
		
	}


}
