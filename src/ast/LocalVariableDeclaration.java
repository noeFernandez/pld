package ast;


import ast.types.AbstractType;
import ast.visitor.Visitor;

/**
 * Created by Noé on 3/12/2015.
 */
public class LocalVariableDeclaration extends Definition {

    public AbstractType type;
    public Integer addr;
    

    public LocalVariableDeclaration(Integer line, Object type, Object identifier){
    	super(line);
        this.type = (AbstractType) type;
        this.identifier = (String) identifier;
    }

    public LocalVariableDeclaration(Integer line, AbstractType type, String identifier) {
        super(line);
        this.type =  type;
        this.identifier =  identifier;
    }


    public String toString(){
        return String.format("Local Variable Definition [AbstractType: %s Identifier: %s]", type, identifier);
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

	@Override
    public AbstractType getType() {
        return this.type;
	}



	@Override
	public Integer getOffset() {
		return this.addr;
	}
	
	@Override
	public void setOffset(Integer offset) {
		this.addr = offset;
		
	}

}
