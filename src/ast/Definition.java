package ast;

import ast.types.AbstractType;

/**
 * Created by Noé on 3/12/2015.
 */
public abstract class Definition extends AST {

	public Integer line;

	public Integer ambito;

	public String identifier;

    public boolean isReference;

	public Definition(Integer line) {
		super(line);
	}

	public abstract AbstractType getType();
	
	public abstract Integer getOffset();
	
	public abstract void setOffset(Integer offset);


     
}
