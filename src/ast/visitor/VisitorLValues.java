package ast.visitor;

import ast.expressions.*;
import ast.sentence.IncrementoStatement;
import ast.types.consts.ConstChar;
import ast.types.consts.FloatConst;
import ast.types.consts.IntConst;

/**
 * Created by Noé on 3/19/2015.
 */
public class VisitorLValues extends Visitor {

    public Object visit(Variable variable, Object param) {
        variable.setLvalue(true);
        return null;
    }

    @Override
    public Object visit(ConstChar node, Object param) {
        node.setLvalue(false);
        return null;
    }

    @Override
    public Object visit(FloatConst node, Object param) {
        node.setLvalue(false);
        return null;
    }

    @Override
    public Object visit(IntConst node, Object param) {
        node.setLvalue(false);
        return null;
    }

    @Override
    public Object visit(BinaryOperation node, Object param) {
        node.right.accept(this, null);
        node.left.accept(this, null);
        node.setLvalue(false);
        return null;
    }

    @Override
    public Object visit(UnaryOperator node, Object param) {
        node.expression.accept(this, null);
        node.setLvalue(false);
        return null;
    }

    @Override
    public Object visit(ArrayAccess node, Object param) {
        node.setLvalue(true);
        return null;
    }

    @Override
    public Object visit(Cast node, Object param) {
        node.setLvalue(false);
        return null;
    }

    @Override
    public Object visit(FieldAccess node, Object param) {
        node.setLvalue(true);
        return null;

    }

    @Override
    public Object visit(FunctionCall node, Object param) {
        node.setLvalue(false);
        return null;
    }

    @Override
    public Object visit(Incremento node, Object param) {
        node.expresionAIncrementar.accept(this, null);
        node.setLvalue(false);
        return null;
    }

    @Override
    public Object visit(IncrementoStatement node, Object param) {
        node.expresionAIncrementar.accept(this, null);
        return null;
    }


}
