package ast.visitor;

import ast.*;
import ast.expressions.*;
import ast.sentence.*;
import ast.types.*;
import ast.types.consts.ConstChar;
import ast.types.consts.FloatConst;
import ast.types.consts.IntConst;
import errors.ME;
import errors.TipoError;

public class InferenciaTipos extends Visitor {

    {
        /**
         * Si la funci�n tiene tipo de retorno void no deber�a tener returns
         * a no ser que se permitan expresiones como return; para salir de las funciones
         * como en Java
         */


    }

    @Override
    public Object visit(ArrayAccess node, Object param) {
        node.accessorExpression.accept(this, null);
        node.fatherExpression.accept(this, null);

        /**
         * Si el nodo padre no es un array entonces acamos un error
         */
        if (!(node.fatherExpression.inferedType instanceof ArrayType)) {
            ME.getME().addError(new TipoError("No es un array", node));
        }

        /**
         * Si lo es, el tipo del nodo ser� el tipo inferido de
         */
        node.inferedType = node.fatherExpression.inferedType.corchetes(node.accessorExpression.inferedType);

        return null;
    }

    @Override
    public Object visit(IncrementoStatement node, Object param) {
        node.expresionAIncrementar.accept(this, null);

        /**
         * Si el tipo devuelto no es instancia de real o entero no se permite hacer el incremento
         */
        if (!(node.expresionAIncrementar.inferedType instanceof IntegerType || node.expresionAIncrementar.inferedType instanceof FloatType)) {
            ME.getME().addError(new TipoError("El operador de incremento solo se puede aplicar a tipos enteros o reales", node));
        }


        /**
         * No se permite aplicar a opeadores que no sean lvalue
         */

        if(!node.expresionAIncrementar.lvalue){
            ME.getME().addError(new TipoError("No se permite usar el operador postfijo sobre expresiones que no sean l-value", node));
        }

        return null;
    }


    @Override
    public Object visit(ConstChar node, Object param) {
        node.inferedType = new CharType();
        return null;
    }

    @Override
    public Object visit(FloatConst node, Object param) {
        node.inferedType = new FloatType();
        return null;
    }

    @Override
    public Object visit(IntConst node, Object param) {
        node.inferedType = new IntegerType();
        return null;
    }

    @Override
    public Object visit(Incremento node, Object param) {
        node.expresionAIncrementar.accept(this, null);

        /**
         * El tipo del incremento es del tipo de lo que se incrementa
         */
        node.inferedType = node.expresionAIncrementar.inferedType;

        /**
         * Si el tipo devuelto no es instancia de real o entero no se permite hacer el incremento
         */
        if (!(node.expresionAIncrementar.inferedType instanceof IntegerType || node.expresionAIncrementar.inferedType instanceof FloatType)) {
            TipoError tp = new TipoError("El operador de incremento solo se puede aplicar a tipos enteros o reales", node);
            ME.getME().addError(tp);
            node.inferedType = tp;
        }

        /**
         * Pero no se permite en literales
         */

        if (!node.expresionAIncrementar.lvalue) {
            TipoError tp = new TipoError("No se permite aplicar el operador postfijo a expresiones que no son un l-value", node);
            ME.getME().addError(tp);
            node.inferedType = tp;
        }

        return null;
    }

    @Override
    public Object visit(Assignment node, Object param) {
        node.left.accept(this, null);
        node.right.accept(this, null);

        /**
         * Si no es un lvalue a�adimos un error
         */
        if (!node.left.lvalue) {
            ME.getME().addError(new TipoError(String.format("%s no es un L-value valido", node.left), node.left));

            /**
             * Si lo es, procedemos a comprobar que sean tipos primitivos
             */
        } else {
            if (node.left.inferedType.isPrimitive() && node.right.inferedType.isPrimitive()) {

                /**
                 * Si son tipos primitivos, se comprueba que el tipo de la
                 * derecha pueda promocionar al tupo de la izquierda
                 */
                if ((node.right.inferedType.promociona(node.left.inferedType) instanceof TipoError)) {
                    ME.getME().addError((TipoError) node.right.inferedType.promociona(node.left.inferedType));
                }

            } else {

                if (node.left.inferedType instanceof TipoError) {
                    ME.getME().addError((TipoError) node.left.inferedType);
                    return null;
                }

                if (node.right.inferedType instanceof TipoError) {
                    ME.getME().addError((TipoError) node.right.inferedType);
                    return null;
                }
                /**
                 * Si no son tipos primitivos sacamos un erroor
                 */
                ME.getME().addError(new TipoError(String.format("Asignaci�n solo permitida entre tipos primitivos"), node.left));
            }

        }

        return null;
    }

    @Override
    public Object visit(Variable variable, Object param) {
        variable.inferedType = variable.definition.getType();
        return null;
    }

    @Override
    public Object visit(FieldAccess node, Object param) {
        node.father.accept(this, null);

        /**
         * Si el "padre" no es una estructura sacamos un error
         */
        if (!(node.father.inferedType instanceof StructType)) {
            ME.getME().addError(new TipoError(String.format("Solo se puede acceder a estructuras [Campo: %s]", node.child), node));
        }

        StructType structure = (StructType) node.father.inferedType;
        FieldDeclaration fieldDeclaration = structure.contieneCampo(node.child);

        if (fieldDeclaration == null) {
            node.inferedType = new TipoError("El campo no existe", node);
            return node.inferedType;
        }

        node.inferedType = fieldDeclaration.type;
        return null;
    }

    @Override
    public Object visit(Cast node, Object param) {
        node.expression.accept(this, null);

        if (!node.expression.inferedType.isPrimitive() || !node.type.isPrimitive()) {
            ME.getME().addError(new TipoError("El cast explicito solo se permite entre tipos primitivos", node));
        }

        node.inferedType = node.expression.inferedType.cast(node.type);

        return null;
    }

    @Override
    public Object visit(Return node, Object param) {
        node.returnExpression.accept(this, null);
        return null;
    }


    public Object visit(FunctionDeclaration node, Object param) {

        node.returnType.accept(this, null);

        for (LocalVariableDeclaration localVariableDeclaration : node.variableDeclarationList) {
            localVariableDeclaration.accept(this, null);
        }

        for (Parameter p : node.paramList) {
            p.accept(this, null);
        }

        for (Sentence s : node.sentenceList) {
            s.accept(this, null);
        }

        if (!(node.returnType instanceof VoidType)) {

            if (node.getReturnSentences().size() > 0) {

                for (Return retur : node.getReturnSentences()) {
                    retur.returnExpression.accept(this, null);

                    if ((retur.returnExpression.inferedType.promociona(node.returnType)) instanceof TipoError) {
                        ME.getME().addError(new TipoError(
                                String.format("La funcion tiene delcarado un tipo de retorno %s," + " pero la sentencia de retorno devuelve %s",
                                        node.returnType, retur.returnExpression.inferedType),
                                retur));
                    }
                }

            } else {
                ME.getME().addError(new TipoError(String.format("La funcion %s tiene declarado el tipo de retorno %s," +
                        " pero no hay ninguna sentencia de retorno", node.identifier, node.returnType.toString()), node));
            }
        } else {

            if (node.getReturnSentences().size() > 0)
                ME.getME().addError(new TipoError(String.format("La funcion %s tiene sentencias de retorno pero no tiene declarado tipo de retorno", node.identifier, node.returnType.toString()), node));
        }

        return null;
    }

    @Override
    public Object visit(While node, Object param) {
        node.condition.accept(this, null);

        for (AST sentence : node.whileSentenceDeclaration) {
            sentence.accept(this, null);
        }

        if ((node.condition.inferedType.promociona(new IntegerType()) instanceof TipoError)) {
            ME.getME().addError(new TipoError("La condicion no es de un tipo valido", node.condition));
        }

        return null;
    }


    @Override
    public Object visit(If node, Object param) {
        node.condition.accept(this, null);

        for (Sentence sentence : node.ifSentenceDeclaration) {
            sentence.accept(this, null);
        }

        for (Sentence sentence : node.elseSentenceDeclaration) {
            sentence.accept(this, null);
        }

        if ((node.condition.inferedType.promociona(new IntegerType()) instanceof TipoError)) {
            ME.getME().addError(new TipoError("La condicion no es de un tipo valido", node.condition));
        }

        return null;
    }

    @Override
    public Object visit(Print node, Object param) {
        for (Expression expression : node.expression) {
            expression.accept(this, null);
            if ((!expression.inferedType.isPrimitive() || expression.inferedType instanceof VoidType) && !(expression.inferedType instanceof TipoError)) {
                ME.getME().addError(new TipoError("Solo se pueden imprimir tipos primitivos", node));
            }
        }

        return null;
    }

    public Object visit(BinaryOperation node, Object param) {

        node.left.accept(this, null);
        node.right.accept(this, null);

        node.inferedType = node.left.inferedType.binaryExpression(node.operator, node.right.inferedType);

        if (node.inferedType instanceof TipoError) {
            ME.getME().addError(new TipoError(((TipoError) node.inferedType).mensaje, node));
        }

        return null;
    }

    public Object visit(UnaryOperator node, Object param) {
        node.expression.accept(this, null);

        node.inferedType = node.expression.inferedType.unaryExpression(node.operator);

        if (node.inferedType instanceof TipoError) {
            ME.getME().addError(new TipoError(((TipoError) node.inferedType).mensaje, node));
        }

        return null;
    }

    @Override
    public Object visit(FunctionCall node, Object param) {

        for (Expression parameter : node.parameterList) {
            parameter.accept(this, null);
        }

        if (node.functionDefinition.checkArguments(node.parameterList) != null) {
            ME.getME().addError((TipoError) node.inferedType);
        }

        node.inferedType = node.functionDefinition.returnType;

        return null;
    }

    @Override
    public Object visit(ProcedureCall node, Object param) {
        for (Expression parameter : node.parameterList) {
            parameter.accept(this, null);
        }

        AbstractType error;

        if ((error = node.functionDefinition.checkArguments(node.parameterList)) != null) {
            ME.getME().addError((TipoError) error);
        }

        return null;
    }

    @Override
    public Object visit(Input node, Object param) {
        for (Expression expression : node.expression) {
            expression.accept(this, null);
            if (!expression.lvalue) {
                ME.getME().addError(new TipoError("La expresion no es un lvalue valido", expression));
            }
        }

        for (Expression expression : node.expression) {

            if (expression.inferedType instanceof TipoError) {
                ME.getME().addError((TipoError) expression.inferedType);
            }

            if (expression.lvalue && !expression.inferedType.isPrimitive() && !(expression.inferedType instanceof TipoError)) {
                ME.getME().addError(new TipoError("Solo se permite la escritura en tipos simples", node));
            }
        }

        return null;
    }
}
