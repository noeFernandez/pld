package ast.visitor;

import ast.VariableDeclaration;


public class TablaSimbolosTest {

    public void testInsertar() {
        TablaSimbolos ts = new TablaSimbolos();
        VariableDeclaration simbolo = new VariableDeclaration(0, null, "a");
        assert (ts.insertar(simbolo));
        assert (ts.buscar(simbolo.identifier).ambito == 0);
        assert (!ts.insertar(simbolo));
        ts.set();
        VariableDeclaration simbolo2 = new VariableDeclaration(0, null, "a");
        assert (ts.insertar(simbolo2));
        assert (ts.buscar(simbolo2.identifier).ambito == 1);
        assert (!ts.insertar(simbolo2));
        ts.reset();
        assert (!ts.insertar(simbolo));
    }

    public void testBuscar() {
        TablaSimbolos ts = new TablaSimbolos();
        VariableDeclaration simbolo = new VariableDeclaration(0, null, "a");
        assert (ts.insertar(simbolo));
        assert (ts.buscar("a") != null);
        assert (ts.buscar("b") == null);
        ts.set();
        VariableDeclaration simbolo2 = new VariableDeclaration(0, null, "b");
        assert (ts.insertar(simbolo2));
        assert (ts.buscar("b") != null);
        assert (ts.buscar("a") != null);
        assert (ts.buscar("c") == null);
        ts.reset();
        assert (ts.buscar("a") != null);
        assert (ts.buscar("b") == null);
    }

    public void testBuscarAmbitoActual() {
        TablaSimbolos ts = new TablaSimbolos();
        VariableDeclaration simbolo = new VariableDeclaration(0, null, "a");
        assert (ts.insertar(simbolo));
        assert (ts.buscarAmbitoActual("a") != null);
        assert (ts.buscarAmbitoActual("b") == null);
        ts.set();
        VariableDeclaration simbolo2 = new VariableDeclaration(0, null, "b");
        assert (ts.insertar(simbolo2));
        assert (ts.buscarAmbitoActual("b") != null);
        assert (ts.buscarAmbitoActual("a") == null);
        assert (ts.buscarAmbitoActual("c") == null);
        ts.reset();
        assert (ts.buscarAmbitoActual("a") != null);
        assert (ts.buscarAmbitoActual("b") == null);
    }

    public static void main(String[] args) {
        TablaSimbolosTest test = new TablaSimbolosTest();
        test.testInsertar();
        test.testBuscar();
        test.testBuscarAmbitoActual();
    }

}
