package ast.visitor;

import ast.*;
import ast.expressions.*;
import ast.sentence.*;
import ast.types.*;
import ast.types.consts.ConstChar;
import ast.types.consts.FloatConst;
import ast.types.consts.IntConst;
import errors.ME;

/**
 * Created by Noé on 3/19/2015.
 */
public class Visitor {

	public Object visit(Program node, Object param) {
		for (Definition definition : node.definitionList) {
			definition.accept(this, null);
		}

		return null;
	}

	/* AbstractType Visitor methods */
	public Object visit(ArrayType node, Object param) {
		return null;
	}

	public Object visit(CharType node, Object param) {
		return null;
	}

	public Object visit(LocalVariableDeclaration node, Object param) {
		return null;
	}

	public Object visit(ConstChar node, Object param) {
		return null;
	}

	public Object visit(FloatConst node, Object param) {
		return null;
	}

	public Object visit(FloatType node, Object param) {
		return null;
	}

	public Object visit(IntConst node, Object param) {
		return null;
	}

	public Object visit(Incremento node, Object param) {
		return null;
	}


	public Object visit(IntegerType node, Object param) {
		return null;
	}

	public Object visit(VoidType node, Object param) {
		return null;
	}

	public Object visit(FieldDeclaration node, Object param) {
		return null;
	}

	public Object visit(IncrementoStatement node, Object param){return null;}

	/* Statement Visitor methods */

	public Object visit(Assignment node, Object param) {
		node.left.accept(this, null);
		node.right.accept(this, null);

		return null;
	}

	public Object visit(If node, Object param) {
		node.condition.accept(this, null);

		for (Sentence sentence : node.ifSentenceDeclaration) {
			sentence.accept(this, null);
		}

		for (Sentence sentence : node.elseSentenceDeclaration) {
			sentence.accept(this, null);
		}

		return null;
	}

	public Object visit(Input node, Object param) {
		for (Expression s : node.expression) {
			s.accept(this, null);
		}
		return null;
	}

	public Object visit(Print node, Object param) {
		for (Expression s : node.expression) {
			s.accept(this, null);
		}

		return null;
	}

	public Object visit(While node, Object param) {
		node.condition.accept(this, null);

		for (AST sentence : node.whileSentenceDeclaration) {
			sentence.accept(this, null);
		}

		return null;
	}

	public Object visit(ProcedureCall node, Object param) {
		for (Expression parameter : node.parameterList) {
			parameter.accept(this, null);
		}

		return null;
	}

	/* Expression visitor methods */

	public Object visit(BinaryOperation node, Object param) {
		node.left.accept(this, null);
		node.right.accept(this, null);

		return null;
	}

	public Object visit(UnaryOperator node, Object param) {
		node.expression.accept(this, null);

		return null;
	}

	public Object visit(ArrayAccess node, Object param) {
		node.accessorExpression.accept(this, null);
		node.fatherExpression.accept(this, null);

		return null;
	}

	public Object visit(Cast node, Object param) {
		node.expression.accept(this, null);
		return null;
	}

	public Object visit(FieldAccess node, Object param) {
		// node.child.accept(this, null);
		node.father.accept(this, null);

		return null;
	}

	public Object visit(FunctionCall node, Object param) {

		for (Expression parameter : node.parameterList) {
			parameter.accept(this, null);
		}

		return null;
	}

	/*  */

	public Object visit(VariableDeclaration node, Object param) {
		return null;
	}

	public Object visit(StructType node, Object param) {
		for (Definition field : node.fieldDeclarationList) {
			field.accept(this, null);
		}

		return null;
	}

	public Object visit(Return node, Object param) {
		node.returnExpression.accept(this, null);
		return null;
	}

	public Object visit(Parameter node, Object param) {
		return null;
	}


	public Object visit(Variable variable, Object param) {
		return null;
	}

	public Object visit(FunctionDeclaration node, Object param) {

		for (Definition vd : node.variableDeclarationList) {
			vd.accept(this, null);
		}

		for (Definition p : node.paramList) {
			p.accept(this, null);
		}

		for (Sentence s : node.sentenceList) {

			s.accept(this, null);
		}

		if (!(node.returnType instanceof VoidType)) {
			if(node.returnExpression != null) {
				node.returnExpression.accept(this, null);
				node.returnExpression.returnExpression.accept(this, null);
			}else{
				/**
				 * Si no hay return y debería, se comprobará en la fase de inferencia de tipos
				 */
			}

		}

		return null;
	}
}
