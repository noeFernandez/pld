package ast.visitor;

import ast.Definition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Noé on 3/26/2015.
 */
public class TablaSimbolos {

    private int ambito = 0;

    private List<Map<String, Definition>> tabla;

    public TablaSimbolos() {
        this.tabla = new ArrayList<Map<String, Definition>>();
        tabla.add(new HashMap<String, Definition>());
    }

    public void set() {
        this.ambito++;
        this.tabla.add(new HashMap<String, Definition>());
    }

    public void reset() {
        tabla.remove(ambito--);
    }

    public boolean insertar(Definition simbolo) {
        if (tabla.get(ambito).get(simbolo.identifier) != null) {
            return false;
        }
        simbolo.ambito = this.ambito;
        tabla.get(ambito).put(simbolo.identifier, simbolo);

        return true;
    }

    public Definition buscar(String id) {
        for (int i = ambito; i >= 0; i--) {
            Definition o = ((Map<String, Definition>) tabla.get(i)).get(id);
            if (o != null) {
//				System.err.println("Simbolo " + o.identifier + " ambito: " + o.ambito);
                return o;
            }
        }
        return null;
    }

    public Definition buscarAmbitoActual(String id) {
        return tabla.get(ambito).get(id);
    }

    public int getAmbito() {
        return this.ambito;
    }

    @Override
    public String toString() {
        return "TablaSimbolos [tabla=" + tabla + "]";
    }


}
