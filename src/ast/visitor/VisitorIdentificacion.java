package ast.visitor;

import ast.*;
import ast.expressions.*;
import ast.sentence.IncrementoStatement;
import ast.sentence.ProcedureCall;
import ast.sentence.Sentence;
import ast.types.StructType;
import ast.types.VoidType;
import errors.ME;
import errors.TipoError;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Noé on 3/26/2015.
 */
public class VisitorIdentificacion extends Visitor {

    private TablaSimbolos tablaVariables;
    private TablaSimbolos tablaFunciones;

    public VisitorIdentificacion() {
        super();

        this.tablaVariables = new TablaSimbolos();
        this.tablaFunciones = new TablaSimbolos();
    }

    @Override
    public Object visit(Incremento node, Object param) {
        node.expresionAIncrementar.accept(this, null);
        return null;
    }

    public Object visit(Program node, Object param) {
        for (Definition definition : node.definitionList) {
            definition.accept(this, null);
        }

        if (tablaFunciones.buscar("main") == null) {
            ME.getME().addError(new TipoError("No hay funcion main", node));
        }

        return null;
    }

    public Object visit(LocalVariableDeclaration node, Object param) {
        node.type.accept(this, null);
        if (!tablaVariables.insertar(node)) {
            ME.getME().addError(
                    new TipoError(
                            String.format("La variable %s ya esta definida",
                                    node.identifier), node));
        }

        return null;
    }

    @Override
    public Object visit(IncrementoStatement node, Object param){
        node.expresionAIncrementar.accept(this, null);
        return null;
    }


    @Override
    public Object visit(FunctionCall node, Object param) {
        if (tablaFunciones.buscar(node.identifier) == null) {
            ME.getME().addError(
                    new TipoError(String.format(
                            "La funcion %s no esta definida", node.identifier),
                            node));

        }

        for (Expression s : node.parameterList) {
            s.accept(this, null);
        }

        node.functionDefinition = (FunctionDeclaration) tablaFunciones
                .buscar(node.identifier);

        return null;
    }

    @Override
    public Object visit(VariableDeclaration node, Object param) {

        node.type.accept(this, null);
        if (!tablaVariables.insertar(node)) {
            ME.getME().addError(
                    new TipoError(
                            String.format("La variable %s ya esta definida",
                                    node.identifier), node));
        }


        return null;
    }

    @Override
    public Object visit(Parameter node, Object param) {
        if (!tablaVariables.insertar(node)) {
            ME.getME().addError(
                    new TipoError(String
                            .format("El parametro %s ya esta definido",
                                    node.identifier), node));
            return null; // ERROR
        }


        return null;
    }

    public Object visit(Cast node, Object param) {
        node.expression.accept(this, null);
        return null;
    }

    @Override
    public Object visit(FieldAccess node, Object param) {
        node.father.accept(this, null);
        return null;
    }

    @Override
    public Object visit(ProcedureCall node, Object param) {
        if (tablaFunciones.buscar(node.identifier) == null) {
            ME.getME().addError(
                    new TipoError(String.format(
                            "La funcion %s no esta definida", node.identifier),
                            node));

        }

        for (Expression s : node.parameterList) {
            s.accept(this, null);
        }

        node.functionDefinition = (FunctionDeclaration) tablaFunciones
                .buscar(node.identifier);

        return null;
    }


    @Override
    public Object visit(StructType node, Object param) {

        for (Definition field : node.fieldDeclarationList) {
            field.accept(this, null);
        }

        // comprobar campos duplicados
        Set<String> fieldSet = new HashSet<String>();

        for (Definition field : node.fieldDeclarationList) {
            if (!fieldSet.add(field.identifier)) {
                ME.getME().addError(
                        new TipoError(String.format(
                                "Campo %s en estructura duplicado",
                                field.identifier), node));
            }
        }

        return null;
    }

    @Override
    public Object visit(Variable node, Object param) {
        if (tablaVariables.buscar(node.identifier) == null) {
            ME.getME().addError(
                    new TipoError(String.format(
                            "El identificador %s no existe", node.identifier),
                            node));
            return null;

        }
        node.definition = tablaVariables.buscar(node.identifier);

        return null;
    }

    @Override
    public Object visit(FunctionDeclaration node, Object param) {
        // metemos la declaraci�n de la funci�n
        if (!tablaFunciones.insertar(node)) {

            ME.getME().addError(
                    new TipoError(String.format(
                            "La funcion %s ya esta definida", node.identifier),
                            node));

        }

        tablaVariables.set();

        for (LocalVariableDeclaration vd : node.variableDeclarationList) {
            vd.accept(this, null);

        }

        for (Definition p : node.paramList) {
            p.accept(this, null);

        }

        for (Sentence s : node.sentenceList) {
            s.accept(this, null);
        }

        if (!(node.returnType instanceof VoidType)) {

            if (node.returnExpression != null) {
                node.returnExpression.accept(this, null);
                node.returnExpression.returnExpression.accept(this, null);
            } else {
                /**
                 * Si no hay return y deber�a, el error se a�adir� en la fase de inferencia
                 */
            }
        }


        tablaVariables.reset();

        return null;
    }

}
