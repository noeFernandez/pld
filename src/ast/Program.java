package ast;

import ast.visitor.Visitor;

import java.util.List;

/**
 * Created by Noé on 3/12/2015.
 */
public class Program extends AST {

    public List<Definition> definitionList;

    public <Object extends List<Definition>> Program(Object definitionList) {
        super(0);
        this.definitionList = definitionList;
    }


    @Override
    public String toString() {
        return String.format("Program| Definitions[%s]", definitionList.toString());
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }
}
