package ast;

import ast.visitor.Visitor;

/**
 * Created by Noé on 3/12/2015.
 */
public abstract class AST {

    public int line;

    public AST(Integer line) {
        this.line = line;
    }

    public abstract Object accept(Visitor visitor, Object param);
}
