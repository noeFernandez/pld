package ast;

import ast.expressions.Expression;
import ast.sentence.Return;
import ast.sentence.Sentence;
import ast.types.AbstractType;
import ast.visitor.Visitor;
import errors.TipoError;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Noé on 3/17/2015.
 */
public class FunctionDeclaration extends Definition {

    public List<LocalVariableDeclaration> variableDeclarationList;
    public List<Parameter> paramList;
    public AbstractType returnType;
    public List<Sentence> sentenceList;
    public Return returnExpression;

    @SuppressWarnings("unchecked")
    public FunctionDeclaration(Integer line, Object identifier,
                               Object variableDeclarationList, Object paramList,
                               Object returnType, Object sentenceList, Object returnExpression) {
        super(line);
        this.identifier = (String) identifier;
        this.variableDeclarationList = (List<LocalVariableDeclaration>) variableDeclarationList;
        this.paramList = (List<Parameter>) paramList;
        this.returnType = (AbstractType) returnType;
        this.sentenceList = (List<Sentence>) sentenceList;
        this.returnExpression = (Return) returnExpression;
    }

    public FunctionDeclaration(Integer line, String identifier,
                               List<LocalVariableDeclaration> variableDeclarationList,
                               List<Parameter> paramList, AbstractType returnType,
                               List<Sentence> sentenceList, Return returnExpression) {
        super(line);
        this.identifier = identifier;
        this.variableDeclarationList = variableDeclarationList;
        this.paramList = paramList;
        this.returnType = returnType;
        this.sentenceList = sentenceList;
        this.returnExpression = returnExpression;
    }

    @SuppressWarnings("unchecked")
    public FunctionDeclaration(Integer line, Object identifier,
                               Object variableDeclarationList, Object paramList,
                               Object returnType, Object sentenceList) {
        super(line);
        this.identifier = (String) identifier;
        this.variableDeclarationList = (List<LocalVariableDeclaration>) variableDeclarationList;
        this.paramList = (List<Parameter>) paramList;
        this.returnType = (AbstractType) returnType;
        this.sentenceList = (List<Sentence>) sentenceList;
    }

    public FunctionDeclaration(Integer line, String identifier,
                               List<LocalVariableDeclaration> variableDeclarationList,
                               List<Parameter> paramList, AbstractType returnType,
                               List<Sentence> sentenceList) {
        super(line);
        this.identifier = identifier;
        this.variableDeclarationList = variableDeclarationList;
        this.paramList = paramList;
        this.returnType = returnType;
        this.sentenceList = sentenceList;
    }


    public List<Return> getReturnSentences() {
        List<Return> temp = new ArrayList<>();
        for (Sentence s : this.sentenceList) {
            if (s instanceof Return) {
                temp.add((Return) s);
            }
        }

        return temp;
    }

    @Override
    public Object accept(Visitor visitor, Object param) {
        return visitor.visit(this, null);
    }

    public AbstractType checkArguments(List<Expression> params) {
        if (this.paramList.size() != params.size()) {
            return new TipoError(
                    String.format(
                            "La funcion %s requiere %d argumentos y le estas pasando %d argumentos",
                            this.identifier, this.paramList.size(),
                            params.size()), this);
        }

        for (int i = 0; i < params.size(); i++) {
            if (!params.get(i).inferedType.isPrimitive()) {
                /**
                 * Si los tipos son iguales hay que comprobar que son iguales, en el caso de los
                 * arrays se comprobará que el tipo y las dimensiones son las mismas,
                 * En el caso de las estructuras han de comprobarse que los campos coinciden
                 */


                AbstractType paramType = params.get(i).inferedType;
                AbstractType functionParamType = this.paramList.get(i).type;


                if (!paramType.equals(functionParamType)) {
                    return new TipoError(String.format("El tipo del parametro pasado no se corresponde con el del parametro esperado %s [tipo : %s]"
                            , this.paramList.get(i).identifier
                            , this.paramList.get(i).type), this)
                            ;
                }

                /**
                 * Se le asigna que es una referencia
                 */
                this.paramList.get(i).isReference = true;
            } else {
                if (!(params.get(i).inferedType.promociona(this.paramList.get(i).type) instanceof TipoError)) {
                    // todo ok
                } else {
                    return new TipoError(String.format("Se ha pasado el tipo %s pero el parametro %s esperaba el tipo %s"
                            , params.get(i).inferedType
                            , this.paramList.get(i).identifier
                            , this.paramList.get(i).type)
                            , this);
                }
            }
        }

        return null;

    }


    public Integer getLocalVariablesSize() {
        int sum = 0;
        for (LocalVariableDeclaration vd : this.variableDeclarationList) {
            sum += vd.type.getSize();
        }

        return sum;
    }

    public Integer getParametersSize() {
        int sum = 0;
        for (Parameter vd : this.paramList) {
            sum += vd.getSize();
        }

        return sum;
    }

    @Override
    public AbstractType getType() {
        return returnExpression.returnExpression.inferedType;
    }

    @Override
    public Integer getOffset() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setOffset(Integer offset) {

    }


}
