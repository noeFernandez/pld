//### This file created by BYACC 1.8(/Java extension  1.14)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";






//#line 2 "sintact.yacc"
    import java.util.*;
    import ast.*;
    import ast.expressions.*;
    import ast.types.*;
    import ast.types.consts.*;
    import ast.sentence.*;
//#line 24 "Parser.java"




public class Parser
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//## **user defined:Object
String   yytext;//user variable to return contextual strings
Object yyval; //used to return semantic vals from action routines
Object yylval;//the 'lval' (result) I got from yylex()
Object valstk[] = new Object[YYSTACKSIZE];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
final void val_init()
{
  yyval=new Object();
  yylval=new Object();
  valptr=-1;
}
final void val_push(Object val)
{
  try {
    valptr++;
    valstk[valptr]=val;
  }
  catch (ArrayIndexOutOfBoundsException e) {
    int oldsize = valstk.length;
    int newsize = oldsize*2;
    Object[] newstack = new Object[newsize];
    System.arraycopy(valstk,0,newstack,0,oldsize);
    valstk = newstack;
    valstk[valptr]=val;
  }
}
final Object val_pop()
{
  return valstk[valptr--];
}
final void val_drop(int cnt)
{
  valptr -= cnt;
}
final Object val_peek(int relative)
{
  return valstk[valptr-relative];
}
//#### end semantic value section ####
public final static short CONSTINT=257;
public final static short INTEGER=258;
public final static short FLOAT=259;
public final static short VOID=260;
public final static short RECORD=261;
public final static short IF=262;
public final static short ELSE=263;
public final static short PRINT=264;
public final static short INPUT=265;
public final static short DO=266;
public final static short MAYOROIGUAL=267;
public final static short BOOLCONST=268;
public final static short IGUAL=269;
public final static short FOR=270;
public final static short DIFERENTE=271;
public final static short MENOROIGUAL=272;
public final static short OR=273;
public final static short AND=274;
public final static short IDENTIFIER=275;
public final static short CONSTFLOAT=276;
public final static short RETURN=277;
public final static short WHILE=278;
public final static short IFSINELSE=279;
public final static short MENOSUNARIO=280;
public final static short MAIN=281;
public final static short CHAR=282;
public final static short CONSTCHAR=283;
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    1,    1,    2,    2,    5,    5,    5,    6,    6,
    6,    8,    7,    9,    9,   10,   10,   10,   11,   11,
   12,   12,    4,    4,    4,    3,    3,   13,   13,   16,
   16,   17,   14,   14,   18,   18,   18,   19,   15,   15,
   20,   20,   21,   21,   21,   21,   21,   21,   21,   28,
   26,   24,   22,   22,   22,   22,   22,   22,   23,   27,
   27,   25,   25,   29,   29,   29,   29,   29,   29,   29,
   29,   29,   29,   29,   29,   29,   29,   29,   29,   29,
   29,   29,   29,   29,   29,   29,   29,   29,
};
final static short yylen[] = {                            2,
    1,    2,    0,    1,    1,    1,    1,    1,    1,    1,
    1,    4,    4,    2,    1,    3,    3,    1,    1,    3,
    2,    0,    3,    3,    3,    9,    9,    1,    0,    3,
    1,    2,    2,    0,    3,    3,    1,    6,    1,    0,
    2,    1,    1,    1,    1,    3,    2,    5,    1,    3,
    3,    3,    7,    5,   11,    9,    7,    9,    8,    1,
    0,    3,    1,    3,    3,    3,    3,    3,    3,    3,
    3,    3,    3,    3,    3,    3,    3,    4,    2,    2,
    3,    4,    3,    1,    1,    1,    1,    4,
};
final static short yydefred[] = {                         3,
    0,    0,    9,   10,    0,    0,   11,    2,    4,    5,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   19,
    0,    0,    0,    0,    0,    0,    0,   15,    0,    0,
   23,    0,   25,   24,    0,    6,    7,    8,    0,    0,
   31,    0,    0,   13,   14,   12,    0,   20,   32,    0,
    0,   16,   17,    0,   34,   30,   34,    0,    0,   84,
    0,    0,    0,    0,    0,   85,    0,    0,   86,    0,
    0,    0,    0,    0,    0,   33,   37,    0,   42,   43,
   44,   45,    0,   49,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,   27,   41,   47,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,   26,    0,    0,    0,   52,    0,   46,    0,    0,
   50,    0,    0,   81,   35,   36,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,   83,    0,    0,    0,    0,    0,    0,   88,
   82,    0,    0,    0,   78,   48,   22,   38,    0,    0,
    0,    0,    0,   57,   21,    0,    0,    0,    0,   59,
    0,   56,   58,    0,   55,
};
final static short yydgoto[] = {                          1,
    2,    8,    9,   10,   11,   24,   37,   26,   27,   28,
   19,  171,   39,   58,   75,   40,   41,   76,   77,   78,
   79,   80,   81,   82,  129,   83,  130,   84,   85,
};
final static short yysindex[] = {                         0,
    0, -127,    0,    0, -273, -118,    0,    0,    0,    0,
  -83, -262, -256, -256,  -13, -236, -200,   19,  -29,    0,
  -18,    6, -236, -256,    0, -256, -105,    0,  -25, -236,
    0, -199,    0,    0,  -81,    0,    0,    0,   39,   42,
    0,   31,   48,    0,    0,    0,   43,    0,    0,  -36,
 -236,    0,    0,  -19,    0,    0,    0,  -33,  -33,    0,
  -12,   54,    4,    4,   73,    0,    4,   77,    0,    4,
    4,   82, -256, -256,   -6,    0,    0,   69,    0,    0,
    0,    0,   76,    0,  427,   13, -236,    4,   97,   64,
  612,  107,    4,  438,    4,  -43,  -43,   98,  464,  123,
  159,    0,    0,    0,    4,    4,    4,    4,    4,    4,
    4,    4,    4,    4,    4,    4,    4,    4,    4,    4,
 -126,    0,  -52,  494,    4,    0,    4,    0,  104,  116,
    0,  505,    4,    0,    0,    0,  882,  798,  798,  882,
  765,  776,  612,  882,  882,   35,   35,   12,   12,   12,
  -43,  527,    0, -256,   -2,  121,  612,  117,   52,    0,
    0,  160,   69,  -79,    0,    0,    0,    0,   55,    7,
  -24,  -63,   69,    0,    0, -256,   58,   29,   80,    0,
   69,    0,    0,   85,    0,
};
final static short yyrindex[] = {                         0,
    0,  217,    0,    0,    0,    0,    0,    0,    0,    0,
    0,  131,  132,  145,    0,    0,    0,  168,    0,    0,
    0,    0,  198,  131,  -80,  145,    0,    0,    0,  198,
    0,    0,    0,    0,    0,    0,    0,    0,    0,  215,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,  142,  142,    0,
    0,    0,    0,    0,  679,    0,    0,    0,    0,    0,
    0,    0,  131,  145,    0,    0,    0,  143,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,   99,    0,
   20,    0,  216,    0,    0,  127,  154,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,  216,    0,    0,    0,  229,    0,
    0,    0,    0,    0,    0,    0,  688,  484,  592,  813,
  -27,   -8,  218,  861,  872,  787,  850,  370,  397,  406,
  343,    0,    0,  187,    0,    0,  106,  743,    0,    0,
    0,    0,  142,   38,    0,    0,    0,    0,    0,    0,
  142,   60,  142,    0,    0,  131,    0,    0,    0,    0,
  142,    0,    0,    0,    0,
};
final static short yygindex[] = {                         0,
    0,    0,    0,  118,    5,   40,    2,   37,  201,  -26,
  864,    0,  262,  239,  -53,    0,  246,    0,    0,    0,
  -54,    0,    0,    0,   -9,    0,  173,    0, 1043,
};
final static int YYTABLESIZE=1176;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                         71,
   45,   15,  121,   13,   16,   86,   72,   17,   71,   17,
    7,   70,   18,   75,   32,   72,   75,   25,   20,   44,
   70,    3,    4,  103,    6,   32,   23,   35,   25,   31,
   71,   75,   74,   75,   35,   74,   71,   72,   14,   71,
   33,   12,   70,   72,   18,    7,   72,  120,   70,   32,
   74,   70,   74,   90,   92,   35,   29,  121,   30,   38,
   63,   71,   36,   63,   34,   75,   38,   46,   72,   36,
   54,  118,  154,   70,   32,   48,  116,   54,   63,   50,
  121,  117,   54,   54,   74,   51,   55,   38,   25,   52,
   36,   32,   53,   88,   74,   74,   45,   73,   73,   53,
  164,   71,  120,   57,   53,  119,   53,  127,   72,  169,
   87,   98,   93,   70,   71,  174,   95,  177,  102,  179,
  163,   72,  126,  182,   25,  120,   70,  184,  119,  173,
    3,    4,    5,    6,  104,   87,  125,  122,  133,   87,
   87,   87,   87,   87,   87,   87,   62,  127,  153,   62,
  127,  181,    3,    4,    7,    6,  158,   87,   87,   87,
   87,  165,   54,   79,   62,  128,   32,   79,   79,   79,
   79,   79,   13,   79,  167,  166,    7,   18,   18,  172,
   18,  135,  180,  170,   53,   79,   79,   79,   79,   87,
   80,   87,   87,   49,   80,   80,   80,   80,   80,  178,
   80,   18,   32,   32,  183,    3,    4,   14,    6,  185,
  176,   19,   80,   80,   80,   80,    1,  136,  168,   79,
   79,    6,    7,   60,    3,    4,   19,   61,   62,    7,
   63,   64,   60,    3,    4,    8,    6,   62,   29,   63,
   64,   65,   66,   67,   68,   75,   80,   80,    7,   69,
   65,   66,   67,   68,   60,   28,   61,    7,   69,   62,
   60,   63,   64,   60,   74,   74,   40,   39,   62,   60,
   63,   64,   65,   66,   67,   68,   51,   13,   89,   66,
   69,   65,   66,   67,   68,   60,   69,  123,  175,   69,
   62,   47,   63,   64,   54,   59,   56,  156,    0,   54,
    0,   54,   54,   65,   66,   67,   68,    0,    0,    0,
    0,   69,   54,   54,   54,   54,   53,    0,    0,    0,
   54,   53,    0,   53,   53,   60,    0,    0,    0,    0,
   62,    0,   63,   64,   53,   53,   53,   53,   60,    3,
    4,    0,   53,   65,   66,   67,   68,    0,    0,    0,
    0,   69,    0,    0,    0,    0,   89,   66,    0,    0,
    0,    0,    0,    7,   69,   87,    0,   87,    0,   87,
   87,   87,   87,    0,    0,    0,    0,    0,    0,   71,
    0,    0,    0,   71,   71,   71,   71,   71,    0,   71,
    0,    0,    0,   79,    0,   79,    0,   79,   79,   79,
   79,   71,   71,   71,   71,    0,   66,    0,    0,    0,
   66,   66,   66,   66,   66,    0,   66,    0,    0,    0,
   80,    0,   80,    0,   80,   80,   80,   80,   66,   66,
   66,   66,    0,   67,    0,   71,   71,   67,   67,   67,
   67,   67,   68,   67,    0,    0,   68,   68,   68,   68,
   68,    0,   68,    0,    0,   67,   67,   67,   67,    0,
    0,    0,   66,  118,   68,   68,   68,   68,  116,  115,
    0,  114,  121,  117,  118,    0,    0,    0,    0,  116,
  115,    0,  114,  121,  117,    0,  112,  111,  113,   67,
    0,    0,    0,    0,    0,    0,  131,  112,   68,  113,
  118,    0,    0,    0,  134,  116,  115,    0,  114,  121,
  117,    0,    0,    0,    0,    0,    0,  120,    0,    0,
  119,    0,    0,  112,   72,  113,    0,   72,  120,    0,
  118,  119,    0,    0,  155,  116,  115,    0,  114,  121,
  117,  118,   72,    0,   72,  159,  116,  115,    0,  114,
  121,  117,    0,  112,  120,  113,    0,  119,    0,    0,
    0,    0,    0,  118,  112,    0,  113,    0,  116,  115,
    0,  114,  121,  117,    0,    0,   72,    0,    0,    0,
    0,    0,    0,    0,  120,    0,  112,  119,  113,    0,
    0,    0,    0,    0,    0,  120,    0,    0,  119,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   71,
    0,   71,    0,   71,   71,   71,   71,  120,    0,  161,
  119,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,   73,    0,    0,   73,   66,    0,   66,    0,
   66,   66,   66,   66,    0,    0,    0,    0,  118,    0,
   73,    0,   73,  116,  115,    0,  114,  121,  117,    0,
    0,    0,    0,   67,    0,   67,    0,   67,   67,   67,
   67,  112,   68,  113,   68,    0,   68,   68,   68,   68,
    0,    0,    0,    0,   73,    0,    0,    0,    0,    0,
    0,    0,    0,  105,    0,  106,    0,  107,  108,  109,
  110,    0,  120,    0,  105,  119,  106,    0,  107,  108,
  109,  110,    0,    0,    0,   87,    0,    0,    0,    0,
   87,   87,    0,   87,   87,   87,    0,    0,   76,    0,
  105,   76,  106,    0,  107,  108,  109,  110,   87,   87,
   87,    0,    0,    0,    0,    0,   76,   76,   76,   76,
    0,    0,   72,    0,   72,    0,   72,   72,    0,    0,
  105,    0,  106,    0,  107,  108,  109,  110,    0,   87,
    0,  105,   87,  106,    0,  107,  108,  109,  110,   78,
   76,    0,    0,    0,   78,   78,    0,   78,   78,   78,
    0,    0,    0,  105,    0,  106,    0,  107,  108,  109,
  110,  118,   78,   78,   78,    0,  116,  115,    0,  114,
  121,  117,  118,    0,    0,    0,    0,  116,  115,    0,
  114,  121,  117,    0,  112,    0,  113,   65,    0,   65,
   65,   65,    0,   78,  118,  112,   78,  113,    0,  116,
  115,    0,  114,  121,  117,   65,   65,   65,   65,    0,
    0,    0,    0,   77,    0,  120,   77,  112,  119,  113,
   73,    0,   73,    0,   73,   73,  120,    0,    0,  119,
    0,   77,   77,   77,   77,    0,   21,   22,  105,   65,
  106,    0,  107,  108,  109,  110,    0,   42,  120,   43,
   64,  119,   64,   64,   64,    0,    0,    0,    0,    0,
    0,   70,    0,    0,   70,   77,    0,    0,   64,   64,
   64,   64,   69,    0,    0,   69,    0,    0,  118,   70,
   70,   70,   70,  116,  115,    0,  114,  121,  117,    0,
   69,   69,   69,   69,    0,    0,  100,  101,    0,    0,
    0,    0,   64,    0,    0,   87,    0,   87,    0,   87,
   87,   87,   87,   70,   76,    0,   76,    0,   76,   76,
   76,   76,    0,    0,   69,    0,    0,    0,    0,    0,
    0,    0,  120,    0,    0,  119,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   78,
    0,   78,    0,   78,   78,   78,   78,  162,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,  105,    0,  106,    0,  107,  108,    0,  110,    0,
    0,    0,  105,    0,  106,    0,  107,  108,    0,    0,
    0,    0,    0,   65,    0,   65,    0,   65,   65,   65,
   65,    0,    0,    0,  105,    0,    0,    0,    0,  108,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   77,
    0,   77,    0,   77,   77,   77,   77,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,   91,   91,    0,    0,   94,
    0,    0,   96,   97,   99,    0,   64,    0,   64,    0,
   64,   64,   64,   64,    0,    0,    0,   70,    0,   70,
  124,   70,   70,   70,   70,   91,    0,  132,   69,    0,
   69,    0,   69,   69,   69,   69,    0,  137,  138,  139,
  140,  141,  142,  143,  144,  145,  146,  147,  148,  149,
  150,  151,  152,    0,    0,    0,    0,   91,    0,  157,
    0,    0,    0,    0,    0,  160,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                         33,
   27,  275,   46,    2,  123,   59,   40,   91,   33,   91,
   91,   45,  275,   41,   44,   40,   44,   16,  275,  125,
   45,  258,  259,   78,  261,   44,   40,   23,   27,   59,
   33,   59,   41,   61,   30,   44,   33,   40,    2,   33,
   59,    2,   45,   40,  125,  282,   40,   91,   45,   44,
   59,   45,   61,   63,   64,   51,  257,   46,   40,   23,
   41,   33,   23,   44,   59,   93,   30,   93,   40,   30,
   33,   37,  125,   45,   44,  275,   42,   40,   59,   41,
   46,   47,   45,   41,   93,   44,  123,   51,   87,   59,
   51,   44,   33,   40,   58,   59,  123,   58,   59,   40,
  155,   33,   91,  123,   45,   94,   59,   44,   40,  163,
  123,   72,   40,   45,   33,  170,   40,  171,  125,  173,
  123,   40,   59,  178,  123,   91,   45,  181,   94,  123,
  258,  259,  260,  261,   59,   37,   40,  125,   41,   41,
   42,   43,   44,   45,   46,   47,   41,   44,  275,   44,
   44,  123,  258,  259,  282,  261,   41,   59,   60,   61,
   62,   41,  125,   37,   59,   59,   44,   41,   42,   43,
   44,   45,  171,   47,  123,   59,  282,  258,  259,  125,
  261,   59,  125,  263,  125,   59,   60,   61,   62,   91,
   37,   93,   94,  275,   41,   42,   43,   44,   45,  263,
   47,  282,   44,   44,  125,  258,  259,  171,  261,  125,
  171,   44,   59,   60,   61,   62,    0,   59,   59,   93,
   94,   91,   91,  257,  258,  259,   59,  261,  262,  282,
  264,  265,  257,  258,  259,   91,  261,  262,   41,  264,
  265,  275,  276,  277,  278,  273,   93,   94,  282,  283,
  275,  276,  277,  278,  257,   41,   41,  282,  283,  262,
  257,  264,  265,  257,  273,  274,  125,  125,  262,   41,
  264,  265,  275,  276,  277,  278,   59,   91,  275,  276,
  283,  275,  276,  277,  278,  257,  283,   87,  171,  283,
  262,   30,  264,  265,  257,   57,   51,  125,   -1,  262,
   -1,  264,  265,  275,  276,  277,  278,   -1,   -1,   -1,
   -1,  283,  275,  276,  277,  278,  257,   -1,   -1,   -1,
  283,  262,   -1,  264,  265,  257,   -1,   -1,   -1,   -1,
  262,   -1,  264,  265,  275,  276,  277,  278,  257,  258,
  259,   -1,  283,  275,  276,  277,  278,   -1,   -1,   -1,
   -1,  283,   -1,   -1,   -1,   -1,  275,  276,   -1,   -1,
   -1,   -1,   -1,  282,  283,  267,   -1,  269,   -1,  271,
  272,  273,  274,   -1,   -1,   -1,   -1,   -1,   -1,   37,
   -1,   -1,   -1,   41,   42,   43,   44,   45,   -1,   47,
   -1,   -1,   -1,  267,   -1,  269,   -1,  271,  272,  273,
  274,   59,   60,   61,   62,   -1,   37,   -1,   -1,   -1,
   41,   42,   43,   44,   45,   -1,   47,   -1,   -1,   -1,
  267,   -1,  269,   -1,  271,  272,  273,  274,   59,   60,
   61,   62,   -1,   37,   -1,   93,   94,   41,   42,   43,
   44,   45,   37,   47,   -1,   -1,   41,   42,   43,   44,
   45,   -1,   47,   -1,   -1,   59,   60,   61,   62,   -1,
   -1,   -1,   93,   37,   59,   60,   61,   62,   42,   43,
   -1,   45,   46,   47,   37,   -1,   -1,   -1,   -1,   42,
   43,   -1,   45,   46,   47,   -1,   60,   61,   62,   93,
   -1,   -1,   -1,   -1,   -1,   -1,   59,   60,   93,   62,
   37,   -1,   -1,   -1,   41,   42,   43,   -1,   45,   46,
   47,   -1,   -1,   -1,   -1,   -1,   -1,   91,   -1,   -1,
   94,   -1,   -1,   60,   41,   62,   -1,   44,   91,   -1,
   37,   94,   -1,   -1,   41,   42,   43,   -1,   45,   46,
   47,   37,   59,   -1,   61,   41,   42,   43,   -1,   45,
   46,   47,   -1,   60,   91,   62,   -1,   94,   -1,   -1,
   -1,   -1,   -1,   37,   60,   -1,   62,   -1,   42,   43,
   -1,   45,   46,   47,   -1,   -1,   93,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   91,   -1,   60,   94,   62,   -1,
   -1,   -1,   -1,   -1,   -1,   91,   -1,   -1,   94,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  267,
   -1,  269,   -1,  271,  272,  273,  274,   91,   -1,   93,
   94,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   41,   -1,   -1,   44,  267,   -1,  269,   -1,
  271,  272,  273,  274,   -1,   -1,   -1,   -1,   37,   -1,
   59,   -1,   61,   42,   43,   -1,   45,   46,   47,   -1,
   -1,   -1,   -1,  267,   -1,  269,   -1,  271,  272,  273,
  274,   60,  267,   62,  269,   -1,  271,  272,  273,  274,
   -1,   -1,   -1,   -1,   93,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,  267,   -1,  269,   -1,  271,  272,  273,
  274,   -1,   91,   -1,  267,   94,  269,   -1,  271,  272,
  273,  274,   -1,   -1,   -1,   37,   -1,   -1,   -1,   -1,
   42,   43,   -1,   45,   46,   47,   -1,   -1,   41,   -1,
  267,   44,  269,   -1,  271,  272,  273,  274,   60,   61,
   62,   -1,   -1,   -1,   -1,   -1,   59,   60,   61,   62,
   -1,   -1,  269,   -1,  271,   -1,  273,  274,   -1,   -1,
  267,   -1,  269,   -1,  271,  272,  273,  274,   -1,   91,
   -1,  267,   94,  269,   -1,  271,  272,  273,  274,   37,
   93,   -1,   -1,   -1,   42,   43,   -1,   45,   46,   47,
   -1,   -1,   -1,  267,   -1,  269,   -1,  271,  272,  273,
  274,   37,   60,   61,   62,   -1,   42,   43,   -1,   45,
   46,   47,   37,   -1,   -1,   -1,   -1,   42,   43,   -1,
   45,   46,   47,   -1,   60,   -1,   62,   41,   -1,   43,
   44,   45,   -1,   91,   37,   60,   94,   62,   -1,   42,
   43,   -1,   45,   46,   47,   59,   60,   61,   62,   -1,
   -1,   -1,   -1,   41,   -1,   91,   44,   60,   94,   62,
  269,   -1,  271,   -1,  273,  274,   91,   -1,   -1,   94,
   -1,   59,   60,   61,   62,   -1,   13,   14,  267,   93,
  269,   -1,  271,  272,  273,  274,   -1,   24,   91,   26,
   41,   94,   43,   44,   45,   -1,   -1,   -1,   -1,   -1,
   -1,   41,   -1,   -1,   44,   93,   -1,   -1,   59,   60,
   61,   62,   41,   -1,   -1,   44,   -1,   -1,   37,   59,
   60,   61,   62,   42,   43,   -1,   45,   46,   47,   -1,
   59,   60,   61,   62,   -1,   -1,   73,   74,   -1,   -1,
   -1,   -1,   93,   -1,   -1,  267,   -1,  269,   -1,  271,
  272,  273,  274,   93,  267,   -1,  269,   -1,  271,  272,
  273,  274,   -1,   -1,   93,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   91,   -1,   -1,   94,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  267,
   -1,  269,   -1,  271,  272,  273,  274,  154,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,  267,   -1,  269,   -1,  271,  272,   -1,  274,   -1,
   -1,   -1,  267,   -1,  269,   -1,  271,  272,   -1,   -1,
   -1,   -1,   -1,  267,   -1,  269,   -1,  271,  272,  273,
  274,   -1,   -1,   -1,  267,   -1,   -1,   -1,   -1,  272,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  267,
   -1,  269,   -1,  271,  272,  273,  274,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   63,   64,   -1,   -1,   67,
   -1,   -1,   70,   71,   72,   -1,  267,   -1,  269,   -1,
  271,  272,  273,  274,   -1,   -1,   -1,  267,   -1,  269,
   88,  271,  272,  273,  274,   93,   -1,   95,  267,   -1,
  269,   -1,  271,  272,  273,  274,   -1,  105,  106,  107,
  108,  109,  110,  111,  112,  113,  114,  115,  116,  117,
  118,  119,  120,   -1,   -1,   -1,   -1,  125,   -1,  127,
   -1,   -1,   -1,   -1,   -1,  133,
};
}
final static short YYFINAL=1;
final static short YYMAXTOKEN=283;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,"'!'",null,null,null,"'%'",null,null,"'('","')'","'*'","'+'",
"','","'-'","'.'","'/'",null,null,null,null,null,null,null,null,null,null,null,
"';'","'<'","'='","'>'",null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,"'['",null,"']'","'^'",null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,"'{'",null,"'}'",null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,"CONSTINT","INTEGER","FLOAT",
"VOID","RECORD","IF","ELSE","PRINT","INPUT","DO","MAYOROIGUAL","BOOLCONST",
"IGUAL","FOR","DIFERENTE","MENOROIGUAL","OR","AND","IDENTIFIER","CONSTFLOAT",
"RETURN","WHILE","IFSINELSE","MENOSUNARIO","MAIN","CHAR","CONSTCHAR",
};
final static String yyrule[] = {
"$accept : program",
"program : declarations",
"declarations : declarations declaration",
"declarations :",
"declaration : functionDeclaration",
"declaration : variableDeclaration",
"type : primitive_type",
"type : structDeclaration",
"type : arrayType",
"primitive_type : INTEGER",
"primitive_type : FLOAT",
"primitive_type : CHAR",
"arrayType : type '[' CONSTINT ']'",
"structDeclaration : RECORD '{' fieldDeclarations '}'",
"fieldDeclarations : fieldDeclarations fieldDeclaration",
"fieldDeclarations : fieldDeclaration",
"fieldDeclaration : primitive_type identifierList ';'",
"fieldDeclaration : arrayType identifierList ';'",
"fieldDeclaration : structDeclaration",
"identifierList : IDENTIFIER",
"identifierList : identifierList ',' IDENTIFIER",
"variableDeclarations : variableDeclarations variableDeclaration",
"variableDeclarations :",
"variableDeclaration : primitive_type identifierList ';'",
"variableDeclaration : arrayType identifierList ';'",
"variableDeclaration : structDeclaration identifierList ';'",
"functionDeclaration : primitive_type IDENTIFIER '(' optionalParamList ')' '{' localVariableDeclarations optionalSentenceList '}'",
"functionDeclaration : VOID IDENTIFIER '(' optionalParamList ')' '{' localVariableDeclarations optionalSentenceList '}'",
"optionalParamList : paramList",
"optionalParamList :",
"paramList : paramList ',' parameter",
"paramList : parameter",
"parameter : type IDENTIFIER",
"localVariableDeclarations : localVariableDeclarations localVariableDeclaration",
"localVariableDeclarations :",
"localVariableDeclaration : primitive_type identifierList ';'",
"localVariableDeclaration : arrayType identifierList ';'",
"localVariableDeclaration : localStructDeclaration",
"localStructDeclaration : RECORD '{' fieldDeclarations '}' identifierList ';'",
"optionalSentenceList : sentenceList",
"optionalSentenceList :",
"sentenceList : sentenceList sentence",
"sentenceList : sentence",
"sentence : conditional",
"sentence : bucle",
"sentence : print",
"sentence : INPUT expresionList ';'",
"sentence : assignement ';'",
"sentence : IDENTIFIER '(' optionalExpresionList ')' ';'",
"sentence : return",
"return : RETURN expresion ';'",
"assignement : expresion '=' expresion",
"print : PRINT expresionList ';'",
"conditional : IF '(' expresion ')' '{' optionalSentenceList '}'",
"conditional : IF '(' expresion ')' sentence",
"conditional : IF '(' expresion ')' '{' optionalSentenceList '}' ELSE '{' optionalSentenceList '}'",
"conditional : IF '(' expresion ')' '{' optionalSentenceList '}' ELSE sentence",
"conditional : IF '(' expresion ')' sentence ELSE sentence",
"conditional : IF '(' expresion ')' sentence ELSE '{' optionalSentenceList '}'",
"bucle : WHILE '(' expresion ')' '{' variableDeclarations optionalSentenceList '}'",
"optionalExpresionList : expresionList",
"optionalExpresionList :",
"expresionList : expresionList ',' expresion",
"expresionList : expresion",
"expresion : expresion '+' expresion",
"expresion : expresion '-' expresion",
"expresion : expresion '*' expresion",
"expresion : expresion '/' expresion",
"expresion : expresion '%' expresion",
"expresion : expresion '>' expresion",
"expresion : expresion '<' expresion",
"expresion : expresion '^' expresion",
"expresion : expresion IGUAL expresion",
"expresion : expresion DIFERENTE expresion",
"expresion : expresion AND expresion",
"expresion : expresion OR expresion",
"expresion : expresion MAYOROIGUAL expresion",
"expresion : expresion MENOROIGUAL expresion",
"expresion : IDENTIFIER '(' optionalExpresionList ')'",
"expresion : '-' expresion",
"expresion : '!' expresion",
"expresion : '(' expresion ')'",
"expresion : expresion '[' expresion ']'",
"expresion : expresion '.' IDENTIFIER",
"expresion : CONSTINT",
"expresion : CONSTFLOAT",
"expresion : CONSTCHAR",
"expresion : IDENTIFIER",
"expresion : '(' primitive_type ')' expresion",
};

//#line 189 "sintact.yacc"
Yylex lex;
AST root;
Parser (Yylex lex) {
this.lex= lex;
}
void yyerror (String s) {
System.out.println ("Error: " +s + this.lex.lexeme() + " Line  " + yyline());
}
int yylex() {
try {
int token= lex.yylex();
yylval= lex.lexeme();
return token;
} catch (Exception e) {
return -1;
} }

public int yyline() {
    return lex.line();
  }

public ArrayType createArray (AbstractType type, Integer size) {
	if (type instanceof ArrayType) {
			// caso tamanio 2
	   ArrayType current = (ArrayType)type;

       AbstractType t = type;
        AbstractType anterior_a_t = null;
        while(t instanceof ArrayType){
            anterior_a_t = t;
            t = ((ArrayType)t).type;
        }


        ArrayType temp = new ArrayType(t,size);
        ((ArrayType)anterior_a_t).type = temp;
	   return current;

	}else
		return new ArrayType (type, size);

}

public AST getAST() {
	return root;
}
//#line 628 "Parser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse()
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 1:
//#line 31 "sintact.yacc"
{root  = new Program((List)val_peek(0));}
break;
case 2:
//#line 34 "sintact.yacc"
{((List)val_peek(1)).addAll((List)val_peek(0));yyval = val_peek(1);}
break;
case 3:
//#line 35 "sintact.yacc"
{yyval = new ArrayList();}
break;
case 4:
//#line 38 "sintact.yacc"
{ yyval = new ArrayList(); ((List)yyval).add(val_peek(0)); }
break;
case 5:
//#line 39 "sintact.yacc"
{ yyval = val_peek(0); }
break;
case 6:
//#line 43 "sintact.yacc"
{yyval = val_peek(0);}
break;
case 7:
//#line 44 "sintact.yacc"
{yyval = new StructType(val_peek(0));}
break;
case 8:
//#line 45 "sintact.yacc"
{yyval = val_peek(0);}
break;
case 9:
//#line 49 "sintact.yacc"
{yyval = new IntegerType();}
break;
case 10:
//#line 50 "sintact.yacc"
{yyval = new FloatType();}
break;
case 11:
//#line 51 "sintact.yacc"
{yyval = new CharType();}
break;
case 12:
//#line 54 "sintact.yacc"
{yyval = createArray((AbstractType)val_peek(3), Integer.parseInt((String)val_peek(1)));}
break;
case 13:
//#line 58 "sintact.yacc"
{yyval = val_peek(1);}
break;
case 14:
//#line 60 "sintact.yacc"
{((List)val_peek(1)).addAll((List)val_peek(0)); yyval = val_peek(1);}
break;
case 15:
//#line 61 "sintact.yacc"
{yyval = val_peek(0);}
break;
case 16:
//#line 64 "sintact.yacc"
{ yyval = new ArrayList(); for (Iterator i = ((List)val_peek(1)).iterator(); i.hasNext(); ) { ((List)yyval).add(new FieldDeclaration(yyline(),val_peek(2), i.next())); } }
break;
case 17:
//#line 65 "sintact.yacc"
{ yyval = new ArrayList(); for (Iterator i = ((List)val_peek(1)).iterator(); i.hasNext(); ) { ((List)yyval).add(new FieldDeclaration(yyline(),val_peek(2), i.next())); } }
break;
case 18:
//#line 66 "sintact.yacc"
{ yyval = val_peek(0); }
break;
case 19:
//#line 70 "sintact.yacc"
{ yyval = new ArrayList(); ((List)yyval).add(val_peek(0)); }
break;
case 20:
//#line 71 "sintact.yacc"
{((List)val_peek(2)).add(val_peek(0)); yyval = val_peek(2);}
break;
case 21:
//#line 75 "sintact.yacc"
{((List)val_peek(1)).addAll((List)val_peek(0)); yyval = val_peek(1);}
break;
case 22:
//#line 76 "sintact.yacc"
{yyval = new ArrayList();}
break;
case 23:
//#line 80 "sintact.yacc"
{ yyval = new ArrayList(); for (Iterator i = ((List)val_peek(1)).iterator(); i.hasNext(); ) { ((List)yyval).add(new VariableDeclaration(yyline(),val_peek(2), i.next())); } }
break;
case 24:
//#line 81 "sintact.yacc"
{ yyval = new ArrayList(); for (Iterator i = ((List)val_peek(1)).iterator(); i.hasNext(); ) { ((List)yyval).add(new VariableDeclaration(yyline(),val_peek(2), i.next())); } }
break;
case 25:
//#line 82 "sintact.yacc"
{ yyval = new ArrayList(); for (Iterator i = ((List)val_peek(1)).iterator(); i.hasNext(); ) { ((List)yyval).add(new VariableDeclaration(yyline(),new StructType(val_peek(2)), i.next())); } }
break;
case 26:
//#line 87 "sintact.yacc"
{yyval = new FunctionDeclaration(yyline(),val_peek(7),val_peek(2),val_peek(5),val_peek(8),val_peek(1)); }
break;
case 27:
//#line 88 "sintact.yacc"
{yyval = new FunctionDeclaration(yyline(),val_peek(7),val_peek(2),val_peek(5),new VoidType(),val_peek(1)); }
break;
case 28:
//#line 91 "sintact.yacc"
{yyval = val_peek(0);}
break;
case 29:
//#line 92 "sintact.yacc"
{yyval = new ArrayList();}
break;
case 30:
//#line 94 "sintact.yacc"
{((List)val_peek(2)).add(val_peek(0)); yyval = val_peek(2);}
break;
case 31:
//#line 95 "sintact.yacc"
{yyval = new ArrayList(); ((List)yyval).add(val_peek(0));}
break;
case 32:
//#line 98 "sintact.yacc"
{yyval = new Parameter(yyline(),val_peek(1),val_peek(0));}
break;
case 33:
//#line 101 "sintact.yacc"
{((List)val_peek(1)).addAll((List)val_peek(0)); yyval = val_peek(1);}
break;
case 34:
//#line 102 "sintact.yacc"
{yyval = new ArrayList();}
break;
case 35:
//#line 105 "sintact.yacc"
{ yyval = new ArrayList(); for (Iterator i = ((List)val_peek(1)).iterator(); i.hasNext(); ) { ((List)yyval).add(new LocalVariableDeclaration(yyline(),val_peek(2), i.next())); } }
break;
case 36:
//#line 106 "sintact.yacc"
{ yyval = new ArrayList(); for (Iterator i = ((List)val_peek(1)).iterator(); i.hasNext(); ) { ((List)yyval).add(new LocalVariableDeclaration(yyline(),val_peek(2), i.next())); } }
break;
case 37:
//#line 107 "sintact.yacc"
{ yyval = val_peek(0); }
break;
case 38:
//#line 110 "sintact.yacc"
{ yyval = new ArrayList(); for (Iterator i = ((List)val_peek(1)).iterator(); i.hasNext(); ) { ((List)yyval).add(new LocalVariableDeclaration(yyline(),new StructType(val_peek(3)), i.next())); } }
break;
case 39:
//#line 114 "sintact.yacc"
{yyval = val_peek(0);}
break;
case 40:
//#line 115 "sintact.yacc"
{yyval = new ArrayList();}
break;
case 41:
//#line 119 "sintact.yacc"
{((List)val_peek(1)).add(val_peek(0));yyval=val_peek(1);}
break;
case 42:
//#line 120 "sintact.yacc"
{yyval = new ArrayList(); ((List)yyval).add(val_peek(0));}
break;
case 43:
//#line 124 "sintact.yacc"
{yyval = val_peek(0);}
break;
case 45:
//#line 126 "sintact.yacc"
{yyval = val_peek(0);}
break;
case 46:
//#line 127 "sintact.yacc"
{yyval = new Input(yyline(),val_peek(1));}
break;
case 47:
//#line 128 "sintact.yacc"
{yyval = val_peek(1);}
break;
case 48:
//#line 129 "sintact.yacc"
{yyval = new ProcedureCall(yyline(),val_peek(4),val_peek(2));}
break;
case 49:
//#line 130 "sintact.yacc"
{yyval = val_peek(0);}
break;
case 50:
//#line 133 "sintact.yacc"
{yyval = new Return(yyline(),(Expression)val_peek(1));}
break;
case 51:
//#line 136 "sintact.yacc"
{yyval = new Assignment(yyline(),val_peek(2),val_peek(0));}
break;
case 52:
//#line 140 "sintact.yacc"
{yyval = new Print(yyline(),val_peek(1));}
break;
case 53:
//#line 141 "sintact.yacc"
{yyval = new If(yyline(),val_peek(4),val_peek(1),null);}
break;
case 54:
//#line 142 "sintact.yacc"
{yyval = new If(yyline(),val_peek(2),val_peek(0),null);}
break;
case 55:
//#line 143 "sintact.yacc"
{yyval = new If(yyline(),val_peek(8),val_peek(5),val_peek(1));}
break;
case 56:
//#line 144 "sintact.yacc"
{yyval = new If(yyline(),val_peek(6),val_peek(3),val_peek(0));}
break;
case 57:
//#line 145 "sintact.yacc"
{yyval = new If(yyline(),val_peek(4),val_peek(2),val_peek(0));}
break;
case 58:
//#line 146 "sintact.yacc"
{yyval = new If(yyline(),val_peek(6),val_peek(4),val_peek(1));}
break;
case 59:
//#line 149 "sintact.yacc"
{yyval = new While(yyline(),val_peek(5),val_peek(2),val_peek(1));}
break;
case 60:
//#line 153 "sintact.yacc"
{yyval = val_peek(0);}
break;
case 61:
//#line 154 "sintact.yacc"
{yyval = new ArrayList();}
break;
case 62:
//#line 156 "sintact.yacc"
{((List)val_peek(2)).add(val_peek(0));yyval =val_peek(2);}
break;
case 63:
//#line 157 "sintact.yacc"
{ yyval = new ArrayList(); ((List)yyval).add(val_peek(0)); }
break;
case 64:
//#line 160 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 65:
//#line 161 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 66:
//#line 162 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 67:
//#line 163 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 68:
//#line 164 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 69:
//#line 165 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 70:
//#line 166 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 71:
//#line 167 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 72:
//#line 168 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 73:
//#line 169 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 74:
//#line 170 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 75:
//#line 171 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 76:
//#line 172 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 77:
//#line 173 "sintact.yacc"
{yyval = new BinaryOperation(yyline(),val_peek(2),val_peek(1),val_peek(0));}
break;
case 78:
//#line 174 "sintact.yacc"
{yyval = new FunctionCall(yyline(),val_peek(3),val_peek(1));}
break;
case 79:
//#line 175 "sintact.yacc"
{yyval = new UnaryOperator(yyline(),val_peek(1),val_peek(0));}
break;
case 80:
//#line 176 "sintact.yacc"
{yyval = new UnaryOperator(yyline(),val_peek(1),val_peek(0));}
break;
case 81:
//#line 177 "sintact.yacc"
{yyval = val_peek(1);}
break;
case 82:
//#line 178 "sintact.yacc"
{yyval = new ArrayAccess(yyline(),val_peek(3),val_peek(1));}
break;
case 83:
//#line 179 "sintact.yacc"
{yyval = new FieldAccess(yyline(),val_peek(2),val_peek(0));}
break;
case 84:
//#line 180 "sintact.yacc"
{yyval = new IntConst(yyline(),val_peek(0));}
break;
case 85:
//#line 181 "sintact.yacc"
{yyval = new FloatConst(yyline(),val_peek(0));}
break;
case 86:
//#line 182 "sintact.yacc"
{yyval = new ConstChar(yyline(),val_peek(0));}
break;
case 87:
//#line 183 "sintact.yacc"
{yyval = new Variable(yyline(),val_peek(0));}
break;
case 88:
//#line 184 "sintact.yacc"
{yyval = new Cast(yyline(),val_peek(0),val_peek(2));}
break;
//#line 1124 "Parser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
/**
 * A default run method, used for operating this parser
 * object in the background.  It is intended for extending Thread
 * or implementing Runnable.  Turn off with -Jnorun .
 */
public void run()
{
  yyparse();
}
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */
public Parser()
{
  //nothing to do
}


/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
public Parser(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################
