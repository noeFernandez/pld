package errors;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class ME {

    private static ME instancia = new ME();
    public boolean failedPorgramCheck;
    private List<TipoError> errores = new ArrayList<TipoError>();

    private ME() {
        this.failedPorgramCheck = false;
    }

    public static ME getME() {
        return instancia;
    }

    public void addError(TipoError error) {
        errores.add(error);
        failedPorgramCheck = true;
    }

    public void mostrarErrores(PrintStream out) {
        for (int i = 0; i < errores.size(); i++)

            out.println(String.format("'\t(%d) => Error: %s \t Codigo [ Linea : %d ]: %s", i + 1, errores.get(i).mensaje, errores.get(i).getNodoAST().line, errores.get(i).getNodoAST().toString()));
    }

    public boolean huboErrores() {
        return errores.size() > 0;
    }

    public void clean() {
        this.errores.clear();
    }
}
