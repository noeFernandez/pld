package errors;

import ast.AST;
import ast.types.AbstractType;
import ast.visitor.Visitor;


public class TipoError extends AbstractType {

	public String mensaje;
	private AST nodoAST;

	public TipoError(String mensaje, AST nodoAST) {
		this.mensaje = mensaje;
		this.nodoAST = nodoAST;
	}

	public String toString() {
		return "Error :" + mensaje;
	}

	@Override
	public Object accept(Visitor visitor, Object param) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isPrimitive() {
		return false;
	}

	public AST getNodoAST() {
		return nodoAST;
	}

	public void setNodoAST(AST nodoAST) {
		this.nodoAST = nodoAST;
	}

	@Override
	public AbstractType promociona(AbstractType other) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractType cast(AbstractType type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractType unaryExpression(String operator) {
		return this;
	}

	@Override
	public AbstractType binaryExpression(String operator, AbstractType param2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractType corchetes(AbstractType indice) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return null;
	}
}
