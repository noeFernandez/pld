%%

%byaccj
%unicode
%line
%column
%public
%standalone

%{

   public String lexeme() {
      return yytext();
    }

  public int line() {
    return yyline + 1;
  }

  public int column() {
    return yycolumn + 1;
  }
%}

digit = [0-9]
letter = [a-zA-Z_]
identifier = {letter}+({letter}|{digit})*
integerConstant= ({digit})+
floatConstant = ({digit})*"."({digit})*
EOF = \n | \r | \r\n
ignorable = {EOF} | [ \t\f]
%%



//Simple characters, such as aritmetic symbols, brackets, etc...
[\^+\-*/%()\[\]:;=!.{}<>,_\"]   { return yycharat(0); }

// data types
int							{return Parser.INTEGER;}
double | float				{return Parser.FLOAT;}
void 						{return Parser.VOID;}
struct						{return Parser.RECORD;}
char | byte                 {return Parser.CHAR;}

// conditionals and loop statements
if							{return Parser.IF;}
else						{return Parser.ELSE;}
while                       {return Parser.WHILE;}
for							{return Parser.FOR;}
do							{return Parser.DO;}


//language functions and reserved words


print						{return Parser.PRINT;}
raw_input					{return Parser.INPUT;}
return                      {return Parser.RETURN;}



// boolean operators
">="						{return Parser.MAYOROIGUAL;}
"=="						{return Parser.IGUAL;}
"!="						{return Parser.DIFERENTE;}
"<="						{return Parser.MENOROIGUAL;}
"||"						{return Parser.OR;}
"&&"						{return Parser.AND;}
"++"                        {return Parser.INCREMENTO;}


{identifier}              {return Parser.IDENTIFIER;}
"'"\\?."'"					   {return Parser.CONSTCHAR;}


//numeric constants
{integerConstant}         {return Parser.CONSTINT;}
{floatConstant}		      {return Parser.CONSTFLOAT;}

			
// any symbol after # character will be treated as a comment and it will be ignored
"#".*	                  { }
// skips these characters
{ignorable}                { }