import ast.AST;
import ast.Program;
import ast.codegeneration.MemoryManagementVisitor;
import ast.codegeneration.VisitorEjecuta;
import ast.visitor.InferenciaTipos;
import ast.visitor.Visitor;
import ast.visitor.VisitorIdentificacion;
import ast.visitor.VisitorLValues;
import errors.ME;
import introspector.model.IntrospectorModel;
import introspector.view.IntrospectorTree;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by Noé on 18/02/2015.
 */
public class Principal {
    public static void main(String[] args) throws IOException {

        if (args.length == 0) {
            System.err
                    .println("Program usage: java -jar compiler.jar sourceFile");
            System.exit(-1);
        }
        File ficheroTest = new File(args[0]);
        FileInputStream fis = new FileInputStream(ficheroTest);
        Yylex lex = new Yylex(fis);
        Parser parser = new Parser(lex);
        int parseRet = parser.yyparse();
        if (parseRet == 0) {
            System.out.println("'Programa sintacticamente correcto");
        } else {
            System.out.println("'Programa sintacticamente incorrecto");
            System.exit(-1);
        }


        AST arbol = parser.getAST();
        Visitor visitorLValues = new VisitorLValues();
        visitorLValues.visit(((Program) arbol), null);
        //
        /** Visitor de identificacion */
        Visitor visitorIdentificacion = new VisitorIdentificacion();
        visitorIdentificacion.visit(((Program) arbol), null);

        if (ME.getME().huboErrores()) {
            System.err.println("'Errores fase identificaci�n");
            ME.getME().mostrarErrores(System.err);
            ME.getME().clean();
        } else {
            System.out.println("'No hubo errores en la fase de identificaci�n");
        }

        Visitor visitorInferencia = new InferenciaTipos();
        visitorInferencia.visit(((Program) arbol), null);
        //
        if (ME.getME().huboErrores()) {
            System.err.println("'Errores fase inferencia de tipos");
            ME.getME().mostrarErrores(System.err);
            ME.getME().clean();
        } else {
            System.out
                    .println("'No hubo errores en la fase fase de inferencia de tipos");
        }


        if (ME.getME().failedPorgramCheck) {
            System.err.println();
            System.err.println();
            System.err.println("****************************COMPILER MESSAGE***********************************");
            System.err.println("*Hubo errores comprobando el programa, solucionelos antes de generar el codigo*");
            System.err.println("*******************************************************************************");

            System.exit(-3);
        }


        Visitor visitorMemoria = new MemoryManagementVisitor();
        visitorMemoria.visit(((Program) arbol), null);
        //
        if (ME.getME().huboErrores()) {
            System.err.println("'Errores fase de gestion de memoria");
            ME.getME().mostrarErrores(System.err);
            ME.getME().clean();
//			System.exit(-1);
        } else {
            System.out
                    .println("'No hubo errores en la fase fase de gestion de memoria");
        }

        if (ME.getME().huboErrores()) {
            ME.getME().mostrarErrores(System.err);
        } else {
            System.out.println("'Programa semanticamente correcto");
        }


        System.out.println("'******************************************");
        System.out.println("'******** CODIGO FUENTE GENERADO **********");
        System.out.println("'******************************************");
        System.out.println();
        System.out.println();
        PrintStream ps = new PrintStream(new File("salida.txt"));
        System.setOut(ps);
        Visitor generacionDeCodigo = new VisitorEjecuta();
        generacionDeCodigo.visit((Program) arbol, null);


    }

    public static void showTree(Object root) {
        IntrospectorModel modelo = new IntrospectorModel("Raíz", root);
        new IntrospectorTree("Introspector", modelo);
    }
}