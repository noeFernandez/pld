%{
    import java.util.*;
    import ast.*;
    import ast.expressions.*;
    import ast.types.*;
    import ast.types.consts.*;
    import ast.sentence.*;
%}

%token CONSTINT INTEGER FLOAT VOID RECORD IF ELSE PRINT INCREMENTO INPUT DO MAYOROIGUAL BOOLCONST IGUAL FOR DIFERENTE MENOROIGUAL OR AND IDENTIFIER CONSTFLOAT  RETURN WHILE IFSINELSE MENOSUNARIO MAIN CHAR CONSTCHAR

/** Precedence rules */
%left OR
%left AND
%left '=' IGUAL  DIFERENTE
%left '<' '>' MAYOROIGUAL MENOROIGUAL
%left '-' '+'
%left '*' '/' '%' 
%left '^'
%right '!' MENOSUNARIO
%right  INCREMENTO
%left '['']'
%left '.'
%nonassoc '(' ')'
%nonassoc IFSINELSE
%nonassoc ELSE


%%

/** General declaration **/
// a program is a set of o declarations
program: declarations;                                                                                                  {root  = new Program((List)$1);}


declarations : declarations declaration                                                                                 {((List)$1).addAll((List)$2);$$ = $1;}
               | /* nothing here */ ;                                                                                   {$$ = new ArrayList();}

// a declaration can be a variable declaration, a function declaration and a type declaration
declaration:  functionDeclaration                                                                                       { $$ = new ArrayList(); ((List)$$).add($1); }
            | variableDeclaration                                                                                       { $$ = $1; }
            ;

/**************** Types declaration ***************/
type:    primitive_type                                                                                                 {$$ = $1;}
  		| structDeclaration                                                                                             {$$ = new StructType($1);}
        | arrayType																										{$$ = $1;}

        ;

primitive_type:     INTEGER                                                                                             {$$ = new IntegerType();}
                  | FLOAT                                                                                               {$$ = new FloatType();}
                  | CHAR                                                                                                {$$ = new CharType();}
                  ;

arrayType: type '['CONSTINT']'                                                                                          {$$ = createArray((AbstractType)$1, Integer.parseInt((String)$3));}
			;

/*********** STRUCT DECLARATION ***********/
structDeclaration : RECORD '{' fieldDeclarations '}' ;                                            					    {$$ = $3;}

fieldDeclarations: fieldDeclarations fieldDeclaration																    {((List)$1).addAll((List)$2); $$ = $1;}
				| fieldDeclaration																					    {$$ = $1;}
				;
				
fieldDeclaration: primitive_type identifierList ';'                                                                     { $$ = new ArrayList(); for (Iterator i = ((List)$2).iterator(); i.hasNext(); ) { ((List)$$).add(new FieldDeclaration(yyline(),$1, i.next())); } }
                     | arrayType identifierList ';'                                                                     { $$ = new ArrayList(); for (Iterator i = ((List)$2).iterator(); i.hasNext(); ) { ((List)$$).add(new FieldDeclaration(yyline(),$1, i.next())); } }
                     | structDeclaration                                                                                { $$ = $1; }
                     ;


identifierList: IDENTIFIER                                                                                              { $$ = new ArrayList(); ((List)$$).add($1); }
                | identifierList ',' IDENTIFIER                                                                         {((List)$1).add($3); $$ = $1;}
                ;

/** Variable delcarations **/
variableDeclarations:     variableDeclarations variableDeclaration                                                      {((List)$1).addAll((List)$2); $$ = $1;}
                        | /* nothing here */                                                                            {$$ = new ArrayList();}
                        ;


variableDeclaration:   primitive_type identifierList ';'                                                                { $$ = new ArrayList(); for (Iterator i = ((List)$2).iterator(); i.hasNext(); ) { ((List)$$).add(new VariableDeclaration(yyline(),$1, i.next())); } }
                     | arrayType identifierList ';'                                                                     { $$ = new ArrayList(); for (Iterator i = ((List)$2).iterator(); i.hasNext(); ) { ((List)$$).add(new VariableDeclaration(yyline(),$1, i.next())); } }
                     | structDeclaration identifierList ';'																{ $$ = new ArrayList(); for (Iterator i = ((List)$2).iterator(); i.hasNext(); ) { ((List)$$).add(new VariableDeclaration(yyline(),new StructType($1), i.next())); } }
                     ;

/*********** FUNCTION DECLARATION *************************/

functionDeclaration : primitive_type IDENTIFIER '(' optionalParamList ')' '{' localVariableDeclarations optionalSentenceList '}'     {$$ = new FunctionDeclaration(yyline(),$2,$7,$4,$1,$8); }
                        | VOID IDENTIFIER '(' optionalParamList ')' '{' localVariableDeclarations optionalSentenceList '}'     {$$ = new FunctionDeclaration(yyline(),$2,$7,$4,new VoidType(),$8); }
                    ;

optionalParamList :  paramList                                                                                          {$$ = $1;}
                    | /* Nothing here */ ;                                                                              {$$ = new ArrayList();}

paramList: paramList ',' parameter                                                                                      {((List)$1).add($3); $$ = $1;}
         | parameter                                                                                                    {$$ = new ArrayList(); ((List)$$).add($1);}
         ;

parameter: type IDENTIFIER ;                                                                                            {$$ = new Parameter(yyline(),$1,$2);}

/** Local variable and struct declaration **/
localVariableDeclarations: localVariableDeclarations localVariableDeclaration                                           {((List)$1).addAll((List)$2); $$ = $1;}
                        | /* nothing here */                                                                            {$$ = new ArrayList();}
                        ;

localVariableDeclaration: primitive_type identifierList ';'                                                             { $$ = new ArrayList(); for (Iterator i = ((List)$2).iterator(); i.hasNext(); ) { ((List)$$).add(new LocalVariableDeclaration(yyline(),$1, i.next())); } }
                     | arrayType identifierList ';'                                                                     { $$ = new ArrayList(); for (Iterator i = ((List)$2).iterator(); i.hasNext(); ) { ((List)$$).add(new LocalVariableDeclaration(yyline(),$1, i.next())); } }
                 | localStructDeclaration                                                                               { $$ = $1; }
                     ;

localStructDeclaration: RECORD '{' fieldDeclarations '}' identifierList ';' ;                                           { $$ = new ArrayList(); for (Iterator i = ((List)$5).iterator(); i.hasNext(); ) { ((List)$$).add(new LocalVariableDeclaration(yyline(),new StructType($3), i.next())); } }


/*** Sentence declaration **/
optionalSentenceList: sentenceList                                                                                      {$$ = $1;}
                    | /* Nothing here */                                                                                {$$ = new ArrayList();}
                    ;


sentenceList : sentenceList sentence                                                                                    {((List)$1).add($2);$$=$1;}
             | sentence                                                                                                 {$$ = new ArrayList(); ((List)$$).add($1);}
             ;


sentence: conditional                                                                                                   {$$ = $1;}
        | bucle
        | print                                                                                                         {$$ = $1;}
        | INPUT expresionList  ';'                                                                                      {$$ = new Input(yyline(),$2);}
        | assignement ';'                                                                                               {$$ = $1;}
        | IDENTIFIER '('optionalExpresionList')'';'                                                                     {$$ = new ProcedureCall(yyline(),$1,$3);}
        | return                                                                                                        {$$ = $1;}
        |  INCREMENTO expresion ';'                                                                                        {$$ = new IncrementoStatement(yyline(), $2);}
        ;

return: RETURN expresion ';'                                                                                            {$$ = new Return(yyline(),(Expression)$2);}


assignement : expresion '=' expresion ;																					{$$ = new Assignment(yyline(),$1,$3);}



print :  PRINT expresionList ';'                                                                                        {$$ = new Print(yyline(),$2);}
conditional:  IF '(' expresion ')' '{'optionalSentenceList '}'                 %prec IFSINELSE                          {$$ = new If(yyline(),$3,$6,null);}
            | IF '(' expresion ')' sentence                                  %prec IFSINELSE                            {$$ = new If(yyline(),$3,$5,null);}
            | IF '(' expresion ')' '{'optionalSentenceList '}' ELSE '{'optionalSentenceList '}'                         {$$ = new If(yyline(),$3,$6,$10);}
            | IF '(' expresion ')' '{'optionalSentenceList '}' ELSE sentence                                            {$$ = new If(yyline(),$3,$6,$9);}
            | IF '(' expresion ')' sentence  ELSE sentence                                                              {$$ = new If(yyline(),$3,$5,$7);}
            | IF '(' expresion ')' sentence ELSE '{'optionalSentenceList '}'                                            {$$ = new If(yyline(),$3,$5,$8);}
            ;

bucle: WHILE '(' expresion ')' '{' variableDeclarations optionalSentenceList '}' ;                                      {$$ = new While(yyline(),$3,$6,$7);}

/** EXPRESIONS **/

optionalExpresionList: expresionList                                                                                    {$$ = $1;}
                     | /* Nothing here */                                                                               {$$ = new ArrayList();}
                     ;
expresionList: expresionList ',' expresion                                                                              {((List)$1).add($3);$$ =$1;}
             | expresion                                                                                                { $$ = new ArrayList(); ((List)$$).add($1); }
             ;

expresion: expresion '+' expresion                                                                                      {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion '-' expresion                                                                                      {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion '*' expresion                                                                                      {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion '/' expresion                                                                                      {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion '%' expresion                                                                                      {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion '>' expresion                                                                                      {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion '<' expresion                                                                                      {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion '^' expresion                                                                                      {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion IGUAL expresion                                                                                    {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion DIFERENTE expresion                                                                                {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion AND expresion                                                                                      {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion OR expresion                                                                                       {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion MAYOROIGUAL expresion                                                                              {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | expresion MENOROIGUAL expresion                                                                              {$$ = new BinaryOperation(yyline(),$1,$2,$3);}
         | IDENTIFIER '('optionalExpresionList')'                                                                       {$$ = new FunctionCall(yyline(),$1,$3);}
         | '-' expresion   %prec MENOSUNARIO                                                                            {$$ = new UnaryOperator(yyline(),$1,$2);}
         | '!' expresion                                                                                                {$$ = new UnaryOperator(yyline(),$1,$2);}
         | '('expresion')'                                                                                              {$$ = $2;}
         | expresion '[' expresion ']'                                                                                  {$$ = new ArrayAccess(yyline(),$1,$3);}
         | expresion '.' IDENTIFIER                                                                                     {$$ = new FieldAccess(yyline(),$1,$3);}
         | CONSTINT                                                                                                     {$$ = new IntConst(yyline(),$1);}
         | CONSTFLOAT                                                                                                   {$$ = new FloatConst(yyline(),$1);}
         | CONSTCHAR                                                                                                    {$$ = new ConstChar(yyline(),$1);}
         | IDENTIFIER                                                                                                   {$$ = new Variable(yyline(),$1);}
         | '('primitive_type')' expresion                                                                               {$$ = new Cast(yyline(),$4,$2);}
         |  INCREMENTO expresion                                                                                        {$$ = new Incremento(yyline(), $2);}
         ;


%%
Yylex lex;
AST root;
Parser (Yylex lex) {
this.lex= lex;
}
void yyerror (String s) {
System.out.println ("Error: " +s + this.lex.lexeme() + " Line  " + yyline());
}
int yylex() {
try {
int token= lex.yylex();
yylval= lex.lexeme();
return token;
} catch (Exception e) {
return -1;
} }

public int yyline() {
    return lex.line();
  }

public ArrayType createArray (AbstractType type, Integer size) {
	if (type instanceof ArrayType) {
			// caso tamanio 2
	   ArrayType current = (ArrayType)type;

       AbstractType t = type;
        AbstractType anterior_a_t = null;
        while(t instanceof ArrayType){
            anterior_a_t = t;
            t = ((ArrayType)t).type;
        }


        ArrayType temp = new ArrayType(t,size);
        ((ArrayType)anterior_a_t).type = temp;
	   return current;

	}else
		return new ArrayType (type, size);

}

public AST getAST() {
	return root;
}